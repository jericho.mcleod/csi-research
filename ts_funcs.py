import numpy as np


#%%


class ts_analyzer():
    
    '''
    This class distills a time series into a measure of similarity across
    one type of defined seasonality that takes into account proximity of 
    observations. 
    
    Comparatively, edit distance between vectors [0,0,0,1] and [0,1,0,0]
    is 2, while for this algorithm it is 0.5, while for vectors [0,0,0,1] and 
    [0,1,0,0] edit distance is also 2, while for this algorithm it is 0.25. 
    This demonstrates the effect of proximity minimally. 
    
    This algorithm will clip data such that only periods containing the full 
    set of seasonal observations are considered.
    
    The nparray variable can be passed in at class instantiation and/or at
    each call of distance method. 
    
    
    Utilization:
        class_inst_name = ts_analyzer() # Instantiate class, pass in params
        class_inst_name.distance(arraylike) # fit to data
        class_inst_name.dist # get distance attribute for last fit
        
    
    Parameters
    -----------
        nparray:        array-like, converts to numpy array. Contains 
                        observations of data to be analyzed in a single array
                        
        period_size:    int, number of observations to a period of seasonality
        
        window:         the number of periods to consider in the distance 
                        measurement. 1 = only compare temporal neighbors, 2 = 
                        compare temporal neighbors and neighbors of neighbors
                        n = compare n neighbor distances (full matrix)
                        
        how:            string, options are:
                            per_obs: divides distance by number of periods and
                                     number of windowed neighbors
                            per_run: divides distance by number of windowed
                                     neighbors
                            all:     total sum of all distances
                            
    Methods
    ------------
        distance:       calculates distance using provided parameters and data
        
    Attributes
    ------------
        dist:           float, contains calculated distance value
        
        per:            int, stores input period_size parameter
        
        n_per:          int, the number of periods observed in the provided 
                        data
        
        data:           np.array, list of np.arrays, stores input data
        
        window:         int, stores input window parameter
        
        how:            str, stores how parameter
    '''
    
    
    def __init__(self, nparray=0, period_size=7, window=1, how = 'per_obs'):
        '''
        Parameters
        ----------
        see class descriptions
        
        Returns
        ----------
        None.

        '''
        
        if type(nparray)==type(0):
            self.per =  period_size
        else:
            if not isinstance(nparray, (np.ndarray, np.generic)):
                try:
                    nparray = np.array(nparray)
                except:
                    print("Could not convert data to np.array")
            self.per =  period_size
            self.n_per = len(nparray)// self.per
            calc_len = int(self.per*self.n_per)
            if len(nparray) != calc_len:
                x = len(nparray) -  calc_len
                print(f'Truncating {x} obs from data.')
                nparray = nparray[0:calc_len]
            self.data = nparray
        self.window = window
        self.how = how

    def distance(self, nparray=0):
        '''
        This method continas the distance measure algorithm as internal 
        functions. On calling, it first updates class attributes to fit any 
        new data, then iteratively calculates distances using a greedy
        algorithm. 
        

        Parameters
        ----------
        nparray:        list, np.array, OPTIONAL
                        see class desc. 

        Returns
        ----------
        None.

        '''
        
        if not isinstance(nparray, (int)):
            self.n_per = len(nparray)// self.per
            calc_len = int(self.per*self.n_per)
            if len(nparray) != calc_len:
                x = len(nparray) -  calc_len
                print(f'Truncating {x} obs from data.')
                nparray = nparray[0:calc_len]
            self.data = nparray
        self.dist = 0
        if type(self.data)!=type([]):
            self.data = np.split(self.data, self.n_per)
        if self.window == 'n':
            self.window = len(self.data)-1
        if self.window > len(self.data)-1:
            self.window = len(self.data)-1
            
            
        def check_pos_neg(a):
            '''
            This function checks whether all values in an array are positive
            or negative or a mixture.

            Parameters
            ----------
            a:      array, ints, data currently being analyzed

            Returns
            -------
                    bool, true a contains positive and negative vals else neg

            '''
            b = [1 if i > 0 else 0 for i in a]
            c = [1 if i < 0 else 0 for i in a]
            return True if sum(b) > 0 and sum(c) > 0 else False
    
        def merges(a,iter_count):
            '''
            This function takes an array of arrays and merges them at an 
            offset of iter_count

            Parameters
            ----------
            a:              numpy array, provided data
            
            iter_count:     int, the iteration currently in progress, limited
                            by window class parameter

            Returns
            -------
            a:              numpy array, provided data, with merged values 
            
            dist:           float, sum of distances values moved in "a" to 
                            reduce overall sum

            '''
            dist = 0
            for i in range(0,len(a)-iter_count):
                b = [a[j] for j in range(i,i+iter_count+1)]
                # This is the key step: it checks whether adding the two values
                # gives a lower abs amount than the sum of the absolute values 
                # AND whether it has a mixture of positive and negative values
                if abs(sum(b)) < sum([abs(j) for j in b]) and check_pos_neg(b):
                    a[i] = 0
                    a[i+iter_count] = sum(b)
                    dist += iter_count
            return(a,dist)
                
        def jedit_dist(a,b):
            '''
            This algorithm merges two arrays by multiplying one by -1 and doing
            pairwise addition as a starting point for the greedy algorithm. 
            Then, while the new array containes a mixture of positive and 
            negative values and the array length limit has not been reached, it 
            attempts to combine values with neighbors to reduce the total abs 
            sum of the array. The iteration count is also the distance at which
            the algorithm is currently checking for pairs, and the distance 
            value that will be divided by length for the current step. 
            Finally, the summation of the absolute values remaining is added to
            the distance measurement. 

            Parameters
            ----------
            a:      numpy array, first array currently being examined
            
            b:      numpy array, second array currently being examined

            Returns
            -------
            dist:   float, the measured distance between a and b 

            '''
            b = [i*-1 for i in b]
            c = [a[i]+b[i] for i in range(len(a))]
            iter_count = 0
            dist = 0
            while check_pos_neg(c) and iter_count < len(c):
                iter_count += 1
                c,d = merges(c,iter_count)
                dist += d
            dist = dist / len(a)
            c = [abs(i) for i in c]
            dist += sum(c)
            return(dist)    
        
        for i in range(self.window):
            for j in range(len(self.data)):
                x = j
                y = j+i+1
                if y > len(self.data)-1:
                    y -= len(self.data)
                d = jedit_dist(self.data[x],self.data[y])
                self.dist += d
                
        if self.how == 'per_obs':
            self.dist /= self.n_per
        if self.how == 'per_obs' or self.how == 'per_run':
            self.dist /= self.window



# test1 = np.array([0,1,0,0,0,0,0,  1,0,0,0,0,1,0,  0,0,1,0,0,1,1])
# test2 = np.array([0,1,0,0,0,0,1,  1,0,0,0,3,0,0,  0,0,0,1,1,0,0,  1])

# ts = ts_analyzer(test1, period_size=7, window = 'n')
# ts.distance()
# print(ts.dist)

# ts.distance(nparray=test2)
# print(ts.dist)
