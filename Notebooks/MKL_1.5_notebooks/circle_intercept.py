
# Circle Intercept plotter

# Imports
import matplotlib.pyplot as plt


# Functions
def vectorize(n):
    x = [i[0] for i in n]
    y = [i[1] for i in n]
    return x, y

def plot_circles(ax, point_1, point_2, r1, r2):
    x,y = vectorize(([point_1, point_2]))
    circ1 = plt.Circle(point_1, r1, fill=False)
    circ2 = plt.Circle(point_2, r2, fill=False)
    ax.add_artist(circ1)
    ax.add_artist(circ2)
    ax.scatter(x,y)
    ax.set_ylim(0,7)
    ax.set_xlim(-1,6)
    return ax

def euclid_d(a, b):
    return ((a[0]-b[0])**2 + (a[1]-b[1])**2)**0.5

def intersect_circ(a, b, r1, r2, d):
    coord = lambda z1, z2, r1, r2, d: (1/2)*(z1+z2) + ((r1**2 - r2**2)/(2*d**2)) * (z2-z1)
    offset = 0.5 * (2 * (r1**2 + r2**2)/d**2 - (r1**2-r2**2)**2 / d**4 -1)**0.5
    y_o = offset * (b[0]-a[0])
    x_o = offset * (a[1]-b[1])
    x = coord(a[0], b[0], r1, r2, d)
    y = coord(a[1], b[1], r1, r2, d)
    coords = [[x+x_o, y+y_o], [x-x_o, y-y_o]]
    return coords


# Data Sample
point_1 = [2,4]
point_2 = [3,2]
r1 = 2
r2 = 1.5

# Computation
D = euclid_d(point_1, point_2)
coords = intersect_circ(point_1, point_2, r1, r2, D)
X, Y = vectorize(coords)

# Plot results
fig, ax = plt.subplots(figsize=(5,5))
plot_circles(ax, point_1, point_2, r1, r2)
ax.scatter(X,Y)