library(dplyr)
library(stargazer)

df <- read.csv("csi/csi-research/Notebooks/cbsa_data_(k,M,f,h,s,p).csv")
df <- dplyr::filter(df, ALL_fpll > 0)
df <- df[, -c(1,2)]
df <- df[, -c(4:29)]
df <- df[, -c(8:19)]

names(df)
#
df_X <- df[, c(1:4)]

reg <- lm(stayed_home_wave2 ~ ALL_fpll + SOCIAL_fpll + CARE_fpll, data=df_X)
summary(reg)

names(df_X)


stargazer(
    reg,
    header = FALSE,
    dep.var.caption = "",
    title = "Impact of Distance from Metro on Price",
    omit.stat = c("ser", "f"),
    no.space = TRUE
)