# -*- coding: utf-8 -*-
"""
Created on Sun Jan 30 15:00:03 2022

@author: Jericho
"""

import matplotlib.pyplot as plt
import numpy as np

def min_slicer(array,min_val):
    '''
    Given an array and a minimum starting value, return two 
    arrays sliced at the first occurrence of a value in the
    input array that is greater or equal to the minimum value
    
    Parameters:
        array: array of int or floats
        min_val: a single int or float
    
    Returns:
        array1: array of values prior to reaching minimum val
        array2: array of values starting on the first occurrence of the min val
    '''
    p = 0
    for i in range(len(array)):
        if array[i] >= min_val:
            p = i
            break
    array1 = array[:p]
    array2 = array[p:]
    return(array1,array2)


def log_vals(array):
    import math
    '''
    Given an array, return an array of the natural logs of the input
    
    Parameters:
        array: array of floats or ints
    
    Returns:
        a: array of ln(array)
    '''
    a = []
    for i in array:
        if i > 0:
            a.append(math.log(i))
        else:
            a.append(0.00001)
    return(a)


def regress(x,y):
    '''
    Given a set of arrays this function will find the coefficients m and b 
    from the system: y = m*x + b That best fit the input
    
    Parameters:
        x: array of floats or ints
        y: array of floats or ints
    
    Returns:
        (m,b): tuple of float values
    '''
    sumxy = 0
    for i in range(len(x)):
        sumxy += x[i] * y[i]
    m = (len(x)*sumxy - sum(x)*sum(y)) / \
        (len(x) * sum([i**2 for i in x]) -  (sum(x))**2)
    b = (sum(y)*(sum([i**2 for i in x])) - \
         sum(x)*sumxy) / (len(x) * sum([i**2 for i in x]) -  (sum(x))**2)
    return((m,b))


def calc_yhat(x,y,m,b):
    '''
    Given a set of arrays and the best regressed values for m and b in the 
    system y = m*x + b, this function will will return an array of the same 
    length as y that represents the y-hat values, or those calculating for y 
    using m*x+b
    
    Parameters:
        x: array of floats or ints
        y: array of floats or ints
        m: float
        b: float
    
    Returns:
        yh: array of floats
    '''
    yh = []
    for i in range(len(x)):
        yhat = x[i]*m + b
        yh.append(yhat)
    assert len(yh) == len(y), 'Error calculating yhat values'
    return(yh)


def calc_rmse(y,yhat):
    '''
    Given two arrays for observed and calculated Y values, this function will 
    return the root mean squared error.
    
    Parameters:
        y:    array of floats or ints
        yhat: array of floats or ints
    
    Returns:
        rmse: float
    '''
    se = 0
    for i in range(len(yhat)):
        se += (yhat[i] - y[i])**2
    mse = se/len(y)
    rmse = mse**0.5
    return(rmse)


def calc_se(y,yhat):
    '''
    Given two arrays for observed and calculated Y values, this function will 
    return an  array of squared error values.
    
    Parameters:
        y:    array of floats or ints
        yhat: array of floats or ints
    
    Returns:
        se:   array of floats
    '''
    se = []
    for i in range(len(yhat)):
        se.append((yhat[i] - y[i])**2)
    return(se)


def find_split(y,yhat,min_p):
    '''
    Given arrays of observed and calculated Y values and
    a minimum length, this function will return the index
    of the position with the greatest error while avoiding
    positions that would result in a minimum partition 
    size below the requirement.
    
    Parameters: 
        y:     array of observed Y values
        yhat:  array of calculated Y values
        min_p: int, minimum partition length
    
    Returns:
        pos:   int value of the index to split on
    
    Note: the split position places the split-on value in 
    the first partition, not the second
    '''
    errmax = 0
    pos = -1
    for i in range(len(y)):
        if i > min_p and i < len(y)-min_p:
            if abs(y[i]-yhat[i]) > errmax:
                errmax = abs(y[i]-yhat[i])
                pos = i
    return(pos)


def make_split(array,pos,overlap):
    '''
    This function takes an array, a position, and a binary
    variable describing whether to allow overlap, and returns
    a set of arrays that are split at that position with the
    indicated overlap.
    
    Parameters:
        array:      array to be split
        pos:        int, index to split on
        overlap:    binary int, 0 = no overlap, 1 = overlap the split-on value
    
    Returns:
        a1,a2:      arrays, input array broken at index 'pos', where the first
                    value of a1 == if last value of a2 if overlap == 1,
                    else the first value of a2 is the value after the last 
                    value in a1 from the perspective of the input array.
    '''
    assert (overlap in [0,1]), 'Invalid overlap value! Use 0 or 1'
    if overlap == 0:
        a1 = array[:pos]
        a2 = array[pos:]
    elif overlap == 1:
        a1 = array[:pos-1+overlap]
        a2 = array[pos-overlap:]
    return(a1,a2)


def merge(array,pos,overlap):
    if overlap == 0:
        new_array = array[pos[0]] + array[pos[1]]
    else:
        new_array = array[pos[0]] + array[pos[1]][1:]
    del array[pos[1]]
    del array[pos[0]]
    array.insert(pos[0],new_array)
    return(array)


def plotter(ys,xs,name):
    import matplotlib.pyplot as plt
    '''
    This function is a custom plotting function. 
    Given a two sets of arrays of arrays, it will plot one
    continuous line over the two sets, and one set of regressed
    straight lines over only the sub-arrays. 
    
    Parameters:
        ys:   array of arrays of Y values
        xs:   array of arrays of X values
        name: the Title to use in the plot
        
    Returns:
        None
    '''
    all_y,all_x = [],[]
    x_flat = []
    y_flat = []
    for i in range(len(ys)):
        all_y += ys[i]
        all_x += xs[i]
        mb = regress(xs[i],ys[i])
        x_flat.append([xs[i][0], xs[i][-1]])
        y_flat.append([xs[i][0]*mb[0]+mb[1], xs[i][-1]*mb[0]+mb[1]] )
    plt.plot(all_x,all_y,linewidth=3)
    plt.title(name)
    for i in range(len(x_flat)):
        plt.plot(x_flat[i],y_flat[i])#,color='#eb8c34')
        

def top_down_alg(data,
                 max_error=0.0001,
                 min_start=2,
                 min_partition_length=10,
                 overlap=1, 
                 plot=False):
    '''
    This function implements to Top-Down portion of the partitioning algorithm.
    Given a vector, it will log the observed values, trim the data,
    and output the set of X and Y array of arrays for the partitioned segments.
    
    It will end when no array has the required RMSE to continue looking for 
    breaking points, or when no array has the required length to allow another 
    split, or when it reaches a specified max iterations. 
    
    Parameters: 
        data:                   array-like, list of observed Y values
        
    Optional Parameters:
        max_error:              Float, the highest RMSE acceptable to end the 
                                loop of looking for breaks
        min_start:              Int or Float, the minimum observed Y value to 
                                begin the algorithm
        min_partition_length:   Int, the minimum length of partitions after 
                                breaks
        overlap: Int Boolean    (0 or 1), whether the end of one partition is 
                                beginning of the next
        plot: Boolean           Generates a plot of data if True
    
    Returns:
        y_array:    Array of arrays, the partitioned observed Y values
        x_array:    Array of arrays, the partitioned array of X values, which 
                    are Y value indices from the input array
        cut:        The index of the first value >= min_start; the length of 
                    trimmed data. 
    '''
    discard, y = min_slicer(data,min_start)
    y = log_vals(y)
    cut = len(discard)
    x = [i+cut for i in range(len(y))]
    y_array = [y]
    x_array = [x]
    mb = regress(x_array[0],y_array[0])
    y_hat = []
    for i in x_array[0]:
        y_hat.append(mb[0]*i+mb[1])
    rmses = [calc_rmse(y_array[0],y_hat)]
    max_rmse = max(rmses)
    iteration_max = 10000
    icount = 1
    while max_rmse > max_error:
        '''Find errors for each subarray in y_obs, then'''
        rmses = []
        for i in range(len(y_array)):
            if len(y_array[i]) > min_partition_length * 2:
                mb = regress(x_array[i],y_array[i])
                y_hat = []
                for j in x_array[i]:
                    y_hat.append(mb[0]*j+mb[1])
                rmse = calc_rmse(y_array[i],y_hat)
                rmses.append(rmse)
            else:
                rmses.append(0)
        max_rmse = max(rmses)
        '''Find split location; if none, break loop'''
        index_of_split_target = rmses.index(max(rmses))
        mb = mb = regress(x_array[index_of_split_target],\
                          y_array[index_of_split_target])
        y_hat = []
        for i in x_array[index_of_split_target]:
            y_hat.append(mb[0]*i+mb[1])
        split_pos = find_split(y_array[index_of_split_target],\
                               y_hat,min_partition_length)
        if split_pos == -1:
            max_rmse = -1
            break
        '''Split the data'''
        y1,y2 = make_split(y_array[index_of_split_target],split_pos,overlap)
        x1,x2 = make_split(x_array[index_of_split_target],split_pos,overlap)
        y_array.insert(index_of_split_target,y1)
        y_array.insert(index_of_split_target+1,y2)
        del y_array[index_of_split_target+2]
        x_array.insert(index_of_split_target,x1)
        x_array.insert(index_of_split_target+1,x2)
        del x_array[index_of_split_target+2]
        iteration_max -=1
        if iteration_max == 0:
            max_rmse=-1
        icount+=1
    if plot == True:
        plotter(y_array,x_array,'Paritioned')
        plt.show()
    '''Return arrays of Y and X values'''
    return(x_array,y_array,cut)


def bottom_up_alg(x,y,overlap,alpha=0.01,plot=False,max_iter = 10):
    '''
    This function iterates over partions, checking to see if neighbors being
    seperated provides a statistically significant reduction of RMSE using an
    F-test. If it does not, the partion is removed. Once an iteration is passed
    with no removed partitions, or the max iterations is reached, the algorithm
    ends. 
    
    Parameters:
        y:          Array of arrays, the partitioned observed Y values
        x:          Array of arrays, the partitioned array of X values, which 
                    are Y value indices from the input array
        overlap:    int boolean (0 or 1), whether the arrays overlap by 1 index
    
    Optional Parameters:
        alpha:      float, significance for F-test
        plot:       Boolean, generates a plot of data if True
        max_iter:   int, maximum number of passes to make over data
    
    Returns: 
        xtemp:      array of arrays, x values departitioned if needed
        ytemp:      array of arrays, y values departitioned if needed
    '''
    
    import copy
    from scipy.stats import f_oneway
    xtemp = copy.deepcopy(x)
    ytemp = copy.deepcopy(y)
    iterations = 10
    while iterations > 0:
        ses = []
        for i in range(len(xtemp)):
            mb = regress(xtemp[i],ytemp[i])
            yhat = calc_yhat(xtemp[i],ytemp[i],mb[0],mb[1])
            se = calc_se(ytemp[i],yhat)
            ses.append(se)
        to_merge = []
        ftests = []
        for i in range(len(xtemp)-1):
            to_merge.append([i,i+1])
            xf = xtemp[i] + xtemp[i+1]
            yf = ytemp[i] + ytemp[i+1]
            sef= ses[i] + ses[i+1]
            mb = regress(xf,yf)
            yhat = calc_yhat(xf,yf,mb[0],mb[1])
            se  = calc_se(yf,yhat)
            f = f_oneway(sef,se)
            ftests.append(f)
        min_f = 9999999999999999
        pos = -1
        for i in range(len(ftests)):
            if ftests[i].pvalue > alpha:
                if ftests[i].statistic < min_f:
                    min_f = ftests[i].statistic
                    pos = i
        if pos > 0:
            xtemp = merge(xtemp,to_merge[pos],overlap)
            ytemp = merge(ytemp,to_merge[pos],overlap)
        else:
            break
        iterations -=1
    if plot == True:
        plotter(ytemp,xtemp,'Departitioned')
        plt.show()
    return(xtemp,ytemp)


def vectorize_slopes(x,y,overlap,cut):
    '''
    This function creates an output array that matches the input array length. 
    Each value in the input array is replaced by the slope of the value's 
    partition determined by the algorithm. Periods prior to the start from the
    min_start value are given 0 values. 
    
    Parameters:
        x:          array, x index values used for regression
        y:          array, y observed values
        overlap:    int Bool, (0 or 1), for whether partions overlap by 1 index
        cut:        the number of values trimmed by min_start parameter
        
    Returns:
        out:        array, slopes
    '''
    out = []
    i_out = []
    slopes = []
    intercepts = []
    for i in range(len(y)):
        mb = regress(x[i],y[i])
        slopes.append(mb[0])
        intercepts.append(mb[1])
    for i in range(len(y)):
        for j in range(len(y[i])-overlap):
            out.append(slopes[i])
            i_out.append(intercepts[i])
    if overlap == 1:
        out.append(slopes[-1])
        i_out.append(intercepts[-1])
    out = [0]*cut + out
    i_out = [0]*cut + i_out
    return(out,i_out)


def local_max(arr,sub,start,exclude=[]):
    start_exc = len(exclude)
    arr_less_sub = [arr[i] for i in range(len(arr)) if i < start]
    sub = list(set(sub))
    arr = list(set(arr))
    test_arr = [-np.inf]+arr+[-np.inf] # avoids exceptions for first/last value
    arr_less_sub = list(set(arr_less_sub))
    maxima = -np.inf
    while len(exclude)-(start_exc-1) < len(sub) and maxima == -np.inf:
        m = max([i for i in sub if i not in exclude])
        ind = arr.index(m)
        if m < test_arr[ind] or m < test_arr[ind+2] or m in arr_less_sub:
            exclude.append(m)
        else: maxima = m
    if maxima < -10*10:
        maxima = np.nan
    return(maxima)


def rule_based_k_finder(array,periods):
    array = array[:periods[-1][-1]]
    w1 = array[periods[0][0]:periods[0][1]]
    w2 = array[periods[1][0]:periods[1][1]]
    w3 = array[periods[2][0]:periods[2][1]]
    m1 = local_max(array,w1,periods[0][0],[])
    m2 = local_max(array,w2,periods[1][0],[m1])
    m3 = local_max(array,w3,periods[2][0],[m1,m2])
    return m1,m2,m3
        

def generate_regressions(y_in,
                         max_error=0.0001,
                         min_start=1,
                         min_partition_length=7,
                         overlap=1,
                         plot=False,
                         alpha=0.01,
                         max_iter = 10,
                         waves = False,
                         periods = [[0,132],[132,235],[235,417]]):
    '''
    Parameters:
        y_in:                   array of floats or integers representing some
                                cumulative quantity in normal scale (not LOG)

    Optional Parameters:
        max_error:              Float, the highest RMSE acceptable to end the 
                                loop of looking for breaks
        min_start:              Int or Float, the minimum observed Y value to 
                                begin the algorithm
        min_partition_length:   Int, the minimum length of partitions after 
                                breaks
        overlap: Int Boolean    (0 or 1), whether the end of one partition is 
                                beginning of the next
        plot: Boolean           Generates a plot of data if True
        alpha:                  float, significance for F-test
        plot:                   Boolean, generates a plot of data if True
        max_iter:               int, maximum number of passes to make over data
        waves:                  Uses the waves identified in the covid-19 JHU 
                                dataset for the first 3 waves to find the max
                                observed slope within a period using a set
                                of logical rules to avoid maxima from prior
                                periods; only a local maxima within a period is
                                counted for a particular period
        periods:                The indices of the waves to use, if not the JHU
                                covid waves

    Returns
        s:      Array, slopes for each y input value according to the 
                partition determined by this script
        ks:     The max observed slope 

    '''
    
    x,y,c  = top_down_alg(y_in,
                          max_error,
                          min_start,
                          min_partition_length,
                          overlap, 
                          plot)
    
    x2,y2 = bottom_up_alg(x,
                          y,
                          overlap, 
                          alpha,
                          plot,
                          max_iter)
    
    s,i = vectorize_slopes(x2,y2,overlap,c)  
    if waves == True:
        k1,k2,k3 = rule_based_k_finder(s,periods=periods)
        ks = [k1,k2,k3]
        return(s,i,ks)
    else:
        return(s,i)


#def unit_test():
#    '''
#    This function is a very simple unit test just simply verifies output length
#    for two test arrays and one set of wave analyses. 
#
#    Returns:
#        None
#
#    '''
#    temp = [0,0,0,0,1,2,2,2,2,3,3,3,4,4,4,4,5,5,6,6,6,7,7,7,8,8,8,9,9,15,27,\
#            33,39,45,51,58,65,72,80,88,95]
#    s,i = generate_regressions(temp,waves=False)
#    assert len(temp) == len(s); 'This script is non-functional'
#    temp2 = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
#              0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,5,
#              6,6,6,6,8,8,10,12,12,12,12,12,12,12,17,18,19,19,19,23,24,24,24,
#              25,26,28,30,32,33,36,36,36,37,39,41,42,43,47,51,54,54,56,58,62,
#              63,72,81,88,90,100,100,108,118,124,130,135,148,151,156,160,171,
#              191,192,204,211,216,227,237,239,241,248,259,264,271,282,295,315,
#              323,334,361,369,371,377,404,415,435,438,447,458,474,480,493,500,
#              505,528,538,554,562,570,584,601,620,623,656,663,670,691,708,736,
#              749,764,785,797,822,850,862,872,885,905,918,939,953,971,988,995,
#              1006,1029,1042,1064,1078,1086,1086,1109,1126,1145,1175,1186,1200,
#              1224,1229,1235,1245,1252,1258,1276,1281,1293,1304,1316,1318,1337,
#              1343,1357,1365,1375,1391,1424,1429,1440,1442,1454,1462,1474,1477,
#              1488,1494,1505,1526,1530,1543,1551,1567,1586,1601,1614,1650,1659,
#              1675,1676,1697,1697,1711,1736,1750,1758,1770,1776,1785,1792,1799,
#              1812,1821,1824,1832,1843,1847,1875,1894,1901,1907,1921,1925,1946,
#              1958,1971,1985,1995,2006,2018,2021,2027,2040,2055,2070,2079,2098,
#              2120,2134,2154,2168,2182,2195,2210,2229,2244,2257,2286,2307,2328,
#              2328,2351,2385,2417,2435,2456,2481,2506,2529,2554,2580,2597,2617,
#              2634,2661,2686,2704,2716,2735,2751,2780,2818,2873,2893,2945,2979,
#              3005,3043,3087,3117,3186,3233,3258,3300,3329,3426,3510,3570,3647,
#              3698,3741,3780,3841,3889,3942,3990,3999,4029,4065,4105,4164,4190,
#              4239,4268,4305,4336,4546,4645,4705,4770,4847,4879,4902,4970,4998,
#              5075,5103,5154,5184,5198,5227,5257,5270,5327,5358,5376,5407,5440,
#              5499,5554,5596,5596,5669,5683,5723,5753,5811,5824,5856,5869,5881,
#              5910,5930,5970,5984,6002,6023,6024,6038,6050,6071,6079,6092,6117,
#              6121,6143,6172,6203,6228,6248,6264,6270,6303,6313,6324,6333,6344,
#              6347,6364,6371,6400,6409,6409,6416,6426,6471,6474]
#    s,i,ks = generate_regressions(temp2,waves=True)
#    assert len(temp2) == len(s); 'This script is non-functional'
#    assert len(ks) == 3; 'This script is non-functional'
#    print('Working as expected')
#
#unit_test()


    
