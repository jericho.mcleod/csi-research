#%%

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns



#%%

def geom_dist_matrix(gdf_path, init_crs=None, proj='esri:102008'):
    import geopandas as gpd
    gdf = gpd.read_file(gdf_path)
    if init_crs is not None:
        gdf = gdf.set_crs(init_crs)
    gdf = gdf.to_crs(proj)

    # distance matrix in miles
    mat = gdf.geometry.centroid.apply(lambda x : (gdf.geometry.centroid.distance(x) / 1000) * 0.621371)
    mat.columns = gdf['GEOID']
    return mat.set_index(gdf['GEOID']).round(2)

def make_cbsa_dist_matrix():
    mat = geom_dist_matrix('../data/census/cb_2020_us_all_5m/cb_2020_us_cbsa_5m/cb_2020_us_cbsa_5m.shp')
    mat.to_csv('../data/clean/cbsa_dist_matrix.csv', index=True)


#%%

def label_bin(df, binned_col, num_bins, quantiles=True):
    temp = df.copy()

    if quantiles:
        # make quantiles
        quantile_size = 1/num_bins
        quantiles = np.round(np.arange(0,1.01,quantile_size),3)
        cutoffs = []
        for q in quantiles:
            cutoff = df[binned_col].quantile(q)
            cutoffs.append(cutoff)
        bins = list(zip(cutoffs,cutoffs[1:]))

        # assign quantile label to each data point for aggregating
        def label_quantile(val):
            right = df[binned_col].max()
            left = df[binned_col].min()
            q = None
            for i,(a,b) in enumerate(bins):
                if val == left: q=1
                elif val >= a and val < b: q = i+1
                elif val == right: q = len(bins) #right most data point
            return q

        temp[f'{binned_col}_bin'] = df[binned_col].apply(lambda x: int(label_quantile(x)))

    else:
        # make equal-sized bins
        temp[f'{binned_col}_bin'], bins = pd.cut(temp[binned_col], num_bins, right=True, retbins=True, labels=range(1, num_bins+1))
        #bins = [round(label,2) for label in bins]
        #bins = list(zip(bins, bins[1:]))

    return temp



def bin(df, binned_col, agg_cols, num_bins=None, quantiles=True, wgt_col=None):
    """
    Bins data into `num_bins` equal-sized bins if `quantiles==False` else into `num_bins` quantile bins.
    `binned_col` is the column in `df` to use in the binning.
    `agg_cols` (list-like) is a container for columns to aggregate in each bin (average and 95% CI).
    """

    temp = df.copy()

    if quantiles:
        # make quantiles
        quantile_size = 1/num_bins
        quantiles = np.round(np.arange(0,1.01,quantile_size),3)
        cutoffs = []
        for q in quantiles:
            cutoff = df[binned_col].quantile(q)
            cutoffs.append(cutoff)
        bins = sorted(set(zip(cutoffs,cutoffs[1:])))

        # assign quantile label to each data point for aggregating
        def label_quantile(val):
            right = df[binned_col].max()
            left = df[binned_col].min()
            q = None
            for i,(a,b) in enumerate(bins):
                if val == left: q=1
                elif val >= a and val < b: q = i+1
                elif val == right: q = len(bins) #right most data point
            return q
        temp['bin'] = df[binned_col].apply(lambda x: int(label_quantile(x)))


    else:
        # make equal-sized bins
        temp['bin'], bins = pd.cut(temp[binned_col], num_bins, right=True, retbins=True, labels=range(1, num_bins+1))
        bins = [round(label,2) for label in bins]
        bins = list(zip(bins, bins[1:]))
    
    bin_labels = [str([round(v1,2),round(v2,2)]) for v1,v2 in bins]
    
    # generate locations on the x-axis for plotting purposes
    midpoints = [round(v1+((v2-v1)/2),2) for v1,v2 in bins]
    xticks = []
    grid_locs = []
    for i in range(len(midpoints)):
        # add ticks at which to display labels (boundaries of each bin and the midpoint)
        xticks.append(round(bins[i][0],2))
        xticks.append(midpoints[i])
        xticks.append(round(bins[i][1],2))
        # add locations to plot grid lines
        grid_locs.append(bins[i][0])
        grid_locs.append(bins[i][1])

    #index = temp.index
    # aggregate within each bin
    if wgt_col is None:
        temp = temp.groupby('bin', as_index=True)[agg_cols].agg(['count','mean','std'])
        #print(temp)
    else:
        temp_copy = temp.copy().reset_index()
        #print(temp_copy)
        wm = lambda x: np.average(x, weights=temp_copy.loc[x.index, wgt_col])
        def weighted_sample_std(x):
            weights=temp_copy.loc[x.index, wgt_col]
            avg = np.average(x, weights=weights)
            #print((weights * (x-avg)))
            std = np.sqrt( np.sum( weights * np.square(x-avg) ) / ( ((len(x)-1)/len(x))*np.sum(weights) ) )
            #print(std)
            #var = np.average((x-avg)**2, weights=weights)
            #var = var*sum(weights)/(sum(weights)-1)
            #return np.sqrt(var)
            return std
        temp = temp.reset_index().groupby('bin', as_index=True)
        #print(temp.index)
        temp = temp[agg_cols].agg(['count', wm, weighted_sample_std])
        #print(temp)
    series = {}
    
    for i,c in enumerate(agg_cols):
        means = []
        cis = []
        stds = []
        ns = []
        for j in temp.index:
            n, mean, std = temp.loc[j][c]
            ci = 1.96 * std / np.sqrt(n)
            means.append(mean)
            cis.append(ci)
            stds.append(std)
            ns.append(n)
        series[f'{c}_mean'] = means
        series[f'{c}_ci'] = cis
        series[f'{c}_std'] = stds
        series[f'{c}_n'] = ns
    
    binned = pd.DataFrame(series)
    #print(binned)
    binned['bin'] = temp.index
    #binned['bin_label'] = bin_labels
    binned['midpoint'] = midpoints

    return binned, xticks, grid_locs




def binned_scatterplot(binned_df, agg_cols, ax, xticks, grid_locs, colors, 
                        xlabel=None, ylabel=None, title=None, legend_loc=None):

    if len(agg_cols) > 1:
        for i,c in enumerate(agg_cols):
            ax.errorbar(binned_df['midpoint'], binned_df[f'{c}_mean'], 
                        yerr=binned_df[f'{c}_ci'], 
                        label=c, capsize=7, markeredgewidth=4, c=colors[i], linewidth=5)
        ax.legend(loc=legend_loc, fontsize='small')
    else:
        ax.errorbar(binned_df['midpoint'], binned_df[f'{agg_cols[0]}_mean'], 
                    yerr=binned_df[f'{agg_cols[0]}_ci'], 
                    label=None, capsize=7, markeredgewidth=4, c=colors[0], linewidth=5)
    
    if xticks is not None:
        ax.set_xticks(grid_locs) # use grid_locs if don't want midpoints
        ax.set_xticklabels(grid_locs, rotation=90, fontsize='medium')
    ax.grid(False)
    for loc in grid_locs[:-1]:
        ax.axvline(loc, ls='--', lw=1, c='lightgrey')
    plt.yticks(fontsize='medium')
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
        
