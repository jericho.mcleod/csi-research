# -*- coding: utf-8 -*-
"""
Created on Mon Jan 31 17:22:25 2022

@author: Jericho
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import os
#os.chdir('C:\\Users\\Jericho\\Documents\\csi_research\\csi-research')
os.chdir('C:\\Users\\Jericho\\Documents\\csi_research\\Dissertation')
from geospatial_functions import *

#%% Sources
'''
Relevant Sources:

COHEN, W.M., R.C. LEVIN and D.C. MOWERY (I 987), “Firm size and R&D intensity: A reexamination”,
journal of Industrial Economics, 35, pp. 543-563.

County distances:
    https://www.nber.org/research/data/county-distance-database

Trips by distance:
    https://www.bts.gov/daily-travel
    
Crosswalk:
    https://www.bls.gov/cew/classifications/areas/county-msa-csa-crosswalk.htm


Census Regions
https://www2.census.gov/geo/pdfs/maps-data/maps/reference/us_regdiv.pdf


KNN + Getis Ord G
    https://journals-sagepub-com.mutex.gmu.edu/doi/10.1177/0023830919894720

Spatial autocorrelation book
    https://www.cambridge.org/core/books/abs/spatial-analysis-methods-and-practice/spatial-autocorrelation/F6A01B574C69076F28318445C33397E4

Moran's I:
    https://www-jstor-org.mutex.gmu.edu/stable/2332142?origin=crossref&seq=1#metadata_info_tab_contents

'''
#%% Data Imports

csa_df = pd.read_csv('CSA_Dataset_Clean.csv')
msa_df = pd.read_csv('MSA_Dataset_Clean.csv')
csa_air = pd.read_csv('csa_airtravel.csv')
msa_air = pd.read_csv('msa_airtravel.csv')

for i in (csa_df,csa_air,msa_df,msa_air):
    del i['Unnamed: 0']
del i

csa_df = pd.merge(csa_df,csa_air,on='GEO',how='left')
msa_df = pd.merge(msa_df,msa_air,on='GEO',how='left')

del csa_air,msa_air

csa_tus = csa_df[csa_df['count_TUS'] >= 10]
msa_tus = msa_df[msa_df['count_TUS'] >= 10]

csa_dists = pd.read_csv('csa_distances.csv')
msa_dists = pd.read_csv('msa_distances.csv')
#msa_dists['Distance'] = pd.to_numeric(msa_dists['Distance'])

csa_dists = csa_dists.pivot(index='GEO1',columns='GEO2',values='Distance')
msa_dists = msa_dists.pivot(index='GEO1',columns='GEO2',values='Distance')

cross = pd.read_csv('county-msa-csa-crosswalk-CUSTOM.csv',encoding='latin-1')
reg   = pd.read_csv('census_regions.csv')

msa_lookup = {}
csa_lookup = {}
regions = {}

for row in cross.iterrows():
    msa = row[1]['MSA Code']
    csa = row[1]['CSA Code']
    fip = row[1]['County Code']
    if type(csa) == type(''):
        if csa in csa_lookup:
            csa_lookup[csa].append(fip)
        else:
            csa_lookup[csa] = [fip]
    if msa in msa_lookup:
        msa_lookup[msa].append(fip)
    else:
        msa_lookup[msa] = [fip]
        
for row in reg.iterrows():
    r = row[1]['Region']
    f = row[1]['State_FIPS']
    regions[f] = r

def find_regions(geo,reg):
    output = {}
    for k,v in geo.items():
        regs = []
        for f in v:
            if len(str(f)) == 5:
                s = str(f)[0:2]
            else:
                s = str(f)[0]
            s = int(s)
            regs.append(reg[s])
        regs = list(set(regs))
        output[k] = regs
    return(output)
        
msa_regions = find_regions(msa_lookup,regions)
csa_regions = find_regions(csa_lookup,regions)

#%% Getis Ord

g_min = 100
g_max = 200
g_step = 5

var_list = ['ts_nh_family']

for var in var_list:
    geo_tus = csa_tus
    geo_dists = csa_dists
    target_dist = 5000
    ms = {'m1': sum([i for i in geo_tus[var].tolist()]),
            'm2': sum([i**2 for i in geo_tus[var].tolist()]),
            'm3': sum([i**3 for i in geo_tus[var].tolist()]),
            'm4': sum([i**4 for i in geo_tus[var].tolist()])}
    
    z_real_hist = []
    z_rand_hist = []
    for i in range(g_min,g_max,g_step):
        print(i)
        target_dist = i
        z = getis(geo_tus,geo_dists,ms,target_dist,var,rand=0)
        z_real_hist.append(z)
        temp_z = []
        for j in range(10):
            z = getis(geo_tus,geo_dists,ms,target_dist,var,rand=1)
            temp_z.append(z)
        z_rand_hist.append(sum(temp_z)/len(temp_z))
    
    x = [i for i in range(g_min,g_max,g_step)]
    
    plt.plot(x,z_real_hist)
    plt.plot(x,z_rand_hist)
    plt.title('CSA - '+var)
    plt.show()
    
    
#%% G_i Statistic

var_strings = ['ts_nh_family']

g_min = 100
g_max = 200
g_step = 5

for var_s in var_strings:
    zs = []
    zrss = []
    ds = []
    
    geos = csa_tus['GEO'].tolist()
    #print(geos)
    
    for d in range(g_min,g_max,g_step):
        #print(d)
        ds.append(d)
        g,e,z = calc_gi(csa_tus,csa_dists,d,var_s)
        
        zr_s = []
        for x in range(20):
            gr,er,zr = calc_rand_gi(csa_tus,csa_dists,d,var_s)
            zr_s += zr
        zs.append(z)
        zrss.append(zr_s)
        
        
#%%

zs_hist = []
zr_hist = []
x = []
for i in range(2,11):
    x.append(i)
    csa_tus_k = kth_n_dist(csa_tus,csa_dists,i)
    GiD,EGi,zs = calc_gi_kth(csa_tus_k,csa_dists,'ts_nh_family')
    zs_hist.append(zs)
    
    zr_temp = []
    for j in range(10):
        GiD,EGi,zr = calc_rand_gi_kth(csa_tus_k,csa_dists,'ts_nh_family')
        zr_temp.append(zr)
    zr_current = []
    for j in range(len(zr_temp[0])):
        zr_current.append(sum([zr_temp[k][j] for k in range(len(zr_temp))])/10)
    zr_hist.append(zr_current)