import matplotlib.pyplot as plt

def done():
    
    '''
    This function plots a large 'DONE' image on a white background so you
    can see that your script execution has completed from a distance. 
    
    
    Parameters:
    -----------
    None
    
    
    Returns:
    -----------
    None
    '''
    
    plt.figure(figsize=(10,10))
    plt.text(0.02,0.4,'DONE',fontsize=(180))
    plt.axis('off')

# TEST:
# done()