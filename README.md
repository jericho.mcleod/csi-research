# CSI Research

This project is for code created in the course of my dissertation research.

# ts_funcs.py

This script contains a class intended to be imported and used as a model. 

The class, ts_analyzer, distills a time series into a measure of similarity across
one type of defined seasonality that takes into account proximity of 
observations. 

Comparatively, edit distance between vectors [0,0,0,1] and [0,1,0,0]
is 2, while for this algorithm it is 0.5, while for vectors [0,0,0,1] and 
[0,1,0,0] edit distance is also 2, while for this algorithm it is 0.25. 
This demonstrates the effect of proximity minimally. 

This algorithm will clip data such that only periods containing the full 
set of seasonal observations are considered.

The nparray variable can be passed in at class instantiation and/or at
each call of distance method. 


Utilization:
```
class_inst_name = ts_analyzer() # Instantiate class, pass in params
class_inst_name.distance(arraylike) # fit to data
class_inst_name.dist # get distance attribute for last fit
```


# done_alert.py

This script plots a large 'DONE' image on a white background so you can see that your script execution has completed from a distance. 

Usage:
```
from done_alert import done

done()
```

# geo_agg.py
Instantiate Class
```python
geo_data = geo_agg()
```
Usage:
To aggregate data directly, call the "agg_data()" method:
```python
geo_data.agg_data( FIPS_list, VALUES_list, Geo_Type)    
```
>FIPS_list = a list of FIPS codes  
>VALUES_list = a list of values to aggregate  
>Geo_Type = a string of 'MSA' or 'CSA' to indicate the aggregation level  

To aggregate normalizing to another array, call the "agg_data_norm()" method:
```python
geo_data.agg_data( FIPS_list, VALUES_list, NORM_list, Geo_Type)    
```
>FIPS_list = a list of FIPS codes  
>VALUES_list = a list of values to aggregate  
>NORM_list = a list of values to weight the aggregation, such as FIPS Population.  
>Geo_Type = a string of 'MSA' or 'CSA' to indicate the aggregation level  

Example run:
```python
f_list = ['01001','01001','01003','01003']  
v_list = [1,2,3,4]  
n_list = [1,1,5,1]  
geo_data.agg_data_norm(f_list,v_list,n_list,'msa')  
print(geo_data.agg_dict)
```

# regress_k.py
This script performs a number of operations given an array of Y values:
1. Trims leading 0s
2. Partions data to minimize error of regressions within partitions
3. Returns an array matching Y in length with associated partition slope values
4. If given periods (or waves) or told to waves matching JHU data, also returns a rule based max-value for the period that is a local maxima. 

The primary function to call, and the default parameters, are:
```
generate_regressions(y_in,
                     max_error=0.0001,
                     min_start=2,
                     min_partition_length=10,
                     overlap=1,
                     plot=False,
                     alpha=0.01,
                     max_iter = 10,
                     waves = True,
                     periods = [[0,132],[132,235],[235,417]]):
```

One may also call a unit test function. It will end in an error or assertion error on fail, otherwise print a success message to the console.
```
unit_test()
> Working as expected
```

