import pandas as pd
import copy
import matplotlib.pyplot as plt
import numpy as np
from bokeh.plotting import figure, show

#%%

cross = pd.read_csv('/Users/jericho/Documents/csi/csi-research/Data/county-cbsa-crosswalk-cleaned.csv')
cross = cross[['County Code','CBSA Code']]
cross.columns = ['FIPS', 'CBSA']
cross['FIPS'] = cross['FIPS'].astype(str).apply(lambda x: x.zfill(5))
cross['CBSA'] = cross['CBSA'].astype(str).apply(lambda x: x.zfill(5))

csob = pd.read_csv('/Users/jericho/Documents/csi/csi-research/Data/county_state_of_birth.csv')
csob['FIPS'] = csob['FIPS'].astype(str).apply(lambda x: x.zfill(5))
csob = csob.merge(cross, on='FIPS', how='left')
csob = csob[~csob['CBSA'].isna()]

cbsa_states = copy.deepcopy(csob)
cbsa_states['state code'] = cbsa_states['FIPS'].astype(str).apply(lambda x: x[0:2])
cbsa_states = cbsa_states[['CBSA','state code']]
cbsa_states = cbsa_states.drop_duplicates()
counts = cbsa_states['CBSA'].value_counts()
cbsa_states['count'] = cbsa_states['CBSA'].apply(lambda x: counts[x])
cbsa_states = cbsa_states[cbsa_states['count']>1]
cbsa_states = cbsa_states[['CBSA','state code']]

cbsa_sob = csob.groupby('CBSA').sum()
cbsa_sob['native'] = cbsa_sob['Born in State'] / cbsa_sob['Population']
cbsa_sob = cbsa_sob.reset_index()
cbsa_sob = cbsa_sob[['CBSA','native']]

cbsa_data = pd.read_csv('/Users/jericho/Documents/csi/csi-research/Data/clean/cbsa/cbsa_summary.csv')
cbsa_data['GTCBSA'] = cbsa_data['GTCBSA'].astype(str).apply(lambda x: x.zfill(5))
cbsa_logpop = cbsa_data[['GTCBSA','LOG_POP']]
cbsa_logpop.columns = ['CBSA','LOG_POP']
cbsa_data = cbsa_data[['GTCBSA','POP','AVG_NHFAMILY_TIME', 'AVG_NHFAMILY_IX', 'AVG_FRIEND_TIME',
                        'AVG_CCC_TIME', 'AVG_NHSOCIAL_TIME', 'AVG_NHFAMILY_SOCIAL_TIME_RATIO',]]
cbsa_data.columns = ['CBSA'] + cbsa_data.columns.tolist()[1:]
cbsa_data = cbsa_data.merge(cbsa_sob,on='CBSA',how='left')

loc = '/Users/jericho/Documents/csi/csi-research/Data/fb_data/'
fb_df = pd.read_csv(loc+'usa_data_narrow.csv')
fb_df.polygon_id = fb_df.polygon_id.astype(str).apply(lambda x: x.zfill(5))
fb_df.ds = fb_df.ds.astype(str).apply(lambda x: x.zfill(10))
fb_df.columns = ['FIPS']+fb_df.columns.tolist()[1:]

fb_df = fb_df.merge(csob, on='FIPS', how='left')
fb_df = fb_df[~fb_df['CBSA'].isna()]
fb_df = fb_df[fb_df['CBSA'].isin(cbsa_data['CBSA'].tolist())]
fb_df['moved_percent'] = 1-fb_df['all_day_ratio_single_tile_users'] 
fb_df['moved_pop_percent'] = fb_df['Population']*fb_df['moved_percent']
fb_df['cbsa_date'] = fb_df['CBSA']+fb_df['ds']

fb_cbsa = fb_df.groupby('cbsa_date').sum()
fb_cbsa['moved_percent'] = fb_cbsa['moved_pop_percent']/fb_cbsa['Population']
fb_cbsa['relative_change'] = fb_cbsa['all_day_bing_tiles_visited_relative_change']/fb_cbsa['Population']
fb_cbsa = fb_cbsa.reset_index()
fb_cbsa['CBSA'] = fb_cbsa['cbsa_date'].apply(lambda x: x[:5])
fb_cbsa['ds'] = fb_cbsa['cbsa_date'].apply(lambda x: x[5:])
fb_cbsa = fb_cbsa[['CBSA','ds','moved_percent','relative_change']]
#%%

def bin_equal(array,bins):
    amin,amax = min(array),max(array)
    step = (amax-amin)/bins
    bins = [(i-amin)//step for i in array]
    mbin = max(bins)
    bin_partitions = [amin+step*i for i in range(int(mbin)+2)]
    return bins, bin_partitions

#%%

bins, partitions = bin_equal(cbsa_logpop['LOG_POP'].tolist(), 6)
cbsa_logpop['popbins'] = bins

fb_binned = fb_cbsa.merge(cbsa_logpop,how='left',on='CBSA')
fb_binned

#%%
x = [1, 2, 3, 4, 5]
y1 = [6, 7, 2, 4, 5]
p = figure(title="Multiple glyphs example", x_axis_label="x", y_axis_label="y")
p.circle(x, y1, legend_label="Temp.", color="blue", size=20)
p.line(x, y1, legend_label="Temp.", color="blue", line_width=2)
show(p)