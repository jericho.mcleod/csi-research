import csv
import urllib
import io


class geo_agg:
    
    def fetch_crosswalk(self):
        data = []
        url_open = urllib.request.urlopen(self.url)
        csvf = csv.reader(io.TextIOWrapper(url_open, encoding = 'ISO-8859-1'), delimiter=',') 
        for row in csvf:
            data.append(row)
        return(data)
    
    def crosswalks(self):
        self.fips_to_msa = {}
        self.fips_to_csa = {}
        for r in self.data:
            fips,msa,csa = r[0],r[2],r[5]
            if msa != '':
                self.fips_to_msa[fips] = msa
            if csa != '':
                self.fips_to_csa[fips] = csa
        return(self.fips_to_msa,self.fips_to_csa)
    
    def __init__(self):
        self.url = 'https://www.bls.gov/cew/classifications/areas/qcew-county-msa-csa-crosswalk-csv.csv'
        self.data = self.fetch_crosswalk()
        self.fips_to_msa,self.fips_to_csa = self.crosswalks()
    
    def set_ref_dict(self,geo_type):
        if geo_type.lower() == 'csa':
            ref_dict = self.fips_to_csa
        elif geo_type.lower() == 'msa':
            ref_dict = self.fips_to_msa
        return(ref_dict)
        
    def agg_data(self,fip:list, val:list, geo_type: str):
        if not type(fip) == type(val) == type([]):
            raise Exception("Use a list for FIPS and Values input")
        if not len(fip) == len(val): 
            raise Exception("Use lists of the same length")
        if geo_type.lower() not in ['csa','msa']: 
            raise Exception("Use 'MSA' or 'CSA' for geo_type")
        self.agg_dict = {}
        ref_dict = self.set_ref_dict(geo_type)
        for i in range(len(fip)):
            if fip[i] in ref_dict:
                geo = ref_dict[fip[i]]
                if geo in self.agg_dict:
                    self.agg_dict[geo] += val[i]
                else:
                    self.agg_dict[geo] = val[i]
        return(self.agg_dict)
        
    def agg_data_norm(self,fip:list, val:list, ref:list, geo_type: str):
        if not type(fip) == type(val) == type(ref) == type([]):
            raise Exception("Use a list for FIPS, Values, and Reference inputs")
        if not len(fip) == len(val) == len(ref):
            raise Exception("Use lists of the same length")
        if geo_type.lower() not in ['csa','msa']:
            raise Exception("Use 'MSA' or 'CSA' for geo_type")
        self.agg_dict = {}
        ref_dict = self.set_ref_dict(geo_type)
        temp_dict = {}
        for i in range(len(fip)):
            if fip[i] in ref_dict:
                geo = ref_dict[fip[i]]
                if geo in temp_dict:
                    temp_dict[geo]['Val'] += val[i]*ref[i]
                    temp_dict[geo]['Ref'] += ref[i]
                else:
                    temp_dict[geo] = {'Val':val[i]*ref[i], 'Ref':ref[i]}
        for k,v in temp_dict.items():
            self.agg_dict[k] = v['Val'] / v['Ref']
        return(self.agg_dict)
    
    