def calc_gi_kth_updated(data_vector,adj_matrix):
    GiD = []
    Wi = []
    EGi = []
    VarG = []
    if type(adj_matrix) != type([]):
        adj_matrix = adj_matrix.values.tolist()
    assert len(data_vector) > 1, 'Data vector dimension must be larger than 1'
    assert len(data_vector) == len(adj_matrix), 'Data and Adjacency dimension mismatch'
    assert len(data_vector) == len(adj_matrix[0]), 'Data and Adjacency dimension mismatch'
    for i in range(len(data_vector)):
        n = len(data_vector)
        adj_vector = adj_matrix[i]
        xjwi = [adj_vector[j] * data_vector[j] if j!=i else 0 for j in range(n)]
        xj = [data_vector[j] if j!=i else 0 for j in range(n)]
        GiD.append(sum(xjwi)/sum(xj))
        Wi.append(sum(adj_vector))
        #print(GiD[-1])
        EGi.append(Wi[-1] / (n-1))
        yi1 = sum(xj)/(n-1)
        yi2 = sum([i**2 for i in xj])/(n-1) - yi1**2
        VG = (Wi[-1]*(n-1-Wi[-1])*yi2) / (((n-1)**2) * (n-2) * (yi1**2))
        VarG.append(VG)
        #print(GiD[-1],EGi[-1],VG,  (GiD[-1]-EGi[-1])/(VG**0.5))
    Z = [(GiD[i]-EGi[i]) / (VarG[i]**0.5) for i in range(len(data_vector))]
    return(GiD,Wi,EGi,VarG,Z)


def calc_local_i(z_values, adj):
    I = []
    EI = []
    Var = []
    Z_results = []
    n = len(z_values)
    m2 = 1 # because z normalization is used; otherwise m2 = sum([j**2/n for j in z_values])
    m4 = sum([i**4/n for i in z_values])
    b2 = m4/m2**2
    for i in range(len(z_values)):
        adj_vector = adj[i]
        adj_vector[i] = 0 # explicitly verifying w_{ii} == 0
        z = z_values[i]
        local_i =  (z/m2) *sum([adj_vector[j]*z_values[j] for j in range(n)])
        I.append(local_i)
        wi = sum(adj_vector)
        ei = -wi/(n-1)
        EI.append(ei)
        wi2 = sum([j**2 for j in adj_vector])
        _2wikh = (wi*(wi-1))/2 # n choose 2
        var_i = (wi2*(n-b2))/(n-1) + (_2wikh*(2*b2-n))/((n-1)*(n-2)) - (wi**2)/((n-1)**2)
        Var.append(var_i)
        z_calc = (local_i - ei) / var_i**0.5
        Z_results.append(z_calc)
    return(I, EI, Var, Z_results)


def filter_dists(dist_df,array,col):
    df_unstacked = dist_df.set_index(col).unstack().reset_index(name='value')
    df_unstacked.columns = [col,'GEOID_2','DISTANCE']
    df_unstacked = df_unstacked[df_unstacked[col].isin(filter_list)]
    df_unstacked = df_unstacked[df_unstacked['GEOID_2'].isin(filter_list)]
    df_restacked = pd.pivot_table(df_unstacked, values = 'DISTANCE',index=col,columns='GEOID_2').reset_index()
    df_restacked.columns = df_restacked.columns.get_level_values(0)
    df_restacked.columns = [col for col in df_restacked.columns.values]
    return df_restacked


def knn_adj_matrix(adj,k):
    adj_matrix = []
    if type(adj) != type([]):
        adj = adj.values.tolist()
    for row in adj:
        threshold = sorted(list(row))[k-1]
        new_row = [1 if i <= threshold else 0 for i in row]
        adj_matrix.append(new_row)
    return adj_matrix


def z_score_array(values):
    value_mean = sum(values) / len(values)
    value_stdev = (sum([(i-value_mean)**2 for i in values])/len(values))**0.5
    z_values = [(i-value_mean) / value_stdev for i in values]
    return z_values