# -*- coding: utf-8 -*-
"""
Created on Mon Sep 27 19:02:42 2021

@author: Jericho
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import copy
import random
from scipy import stats
import os
os.chdir('C:\\Users\\Jericho\\Documents\\csi_research\\Dissertation')
#import geopandas


#%% Citations

'''
Citation:
COHEN, W.M., R.C. LEVIN and D.C. MOWERY (I 987), “Firm size and R&D intensity: A reexamination”,
journal of Industrial Economics, 35, pp. 543-563.

County distances:
    https://www.nber.org/research/data/county-distance-database

Trips by distance:
    https://www.bts.gov/daily-travel
    
Crosswalk:
    https://www.bls.gov/cew/classifications/areas/county-msa-csa-crosswalk.htm


Census Regions
https://www2.census.gov/geo/pdfs/maps-data/maps/reference/us_regdiv.pdf


KNN + Getis Ord G
    https://journals-sagepub-com.mutex.gmu.edu/doi/10.1177/0023830919894720

Spatial autocorrelation book
    https://www.cambridge.org/core/books/abs/spatial-analysis-methods-and-practice/spatial-autocorrelation/F6A01B574C69076F28318445C33397E4

Moran's I:
    https://www-jstor-org.mutex.gmu.edu/stable/2332142?origin=crossref&seq=1#metadata_info_tab_contents

'''
#%% Data Imports

csa_df = pd.read_csv('CSA_Dataset_Clean.csv')
msa_df = pd.read_csv('MSA_Dataset_Clean.csv')
csa_air = pd.read_csv('csa_airtravel.csv')
msa_air = pd.read_csv('msa_airtravel.csv')

for i in (csa_df,csa_air,msa_df,msa_air):
    del i['Unnamed: 0']
del i

csa_df = pd.merge(csa_df,csa_air,on='GEO',how='left')
msa_df = pd.merge(msa_df,msa_air,on='GEO',how='left')

del csa_air,msa_air

csa_tus = csa_df[csa_df['count_TUS'] >= 10]
msa_tus = msa_df[msa_df['count_TUS'] >= 10]

csa_dists = pd.read_csv('csa_distances.csv')
msa_dists = pd.read_csv('msa_distances.csv')
#msa_dists['Distance'] = pd.to_numeric(msa_dists['Distance'])

csa_dists = csa_dists.pivot(index='GEO1',columns='GEO2',values='Distance')
msa_dists = msa_dists.pivot(index='GEO1',columns='GEO2',values='Distance')

#%% Import Regions

cross = pd.read_csv('county-msa-csa-crosswalk-CUSTOM.csv',encoding='latin-1')
reg   = pd.read_csv('census_regions.csv')

msa_lookup = {}
csa_lookup = {}
regions = {}

for row in cross.iterrows():
    msa = row[1]['MSA Code']
    csa = row[1]['CSA Code']
    fip = row[1]['County Code']
    if type(csa) == type(''):
        if csa in csa_lookup:
            csa_lookup[csa].append(fip)
        else:
            csa_lookup[csa] = [fip]
    if msa in msa_lookup:
        msa_lookup[msa].append(fip)
    else:
        msa_lookup[msa] = [fip]
        
for row in reg.iterrows():
    r = row[1]['Region']
    f = row[1]['State_FIPS']
    regions[f] = r

def find_regions(geo,reg):
    output = {}
    for k,v in geo.items():
        regs = []
        for f in v:
            if len(str(f)) == 5:
                s = str(f)[0:2]
            else:
                s = str(f)[0]
            s = int(s)
            regs.append(reg[s])
        regs = list(set(regs))
        output[k] = regs
    return(output)
        
msa_regions = find_regions(msa_lookup,regions)
csa_regions = find_regions(csa_lookup,regions)


#msa_ref = msa_dists.columns.tolist()
#print(msa_dists.iloc[msa_ref.index('C1010')]['C1014'])

#%% G Statistic

def rand_array(a):
    b = []
    while len(a) > 0:
        b.append(a.pop(random.randint(0,len(a)-1)))
        
    for i in range(len(a)):
        print(a[i],b[i])
    return b

def nr(n,r): #todo verify this
    x = 1
    for i in range(1,r+1):
        x = x*(n-i+1)
    return x

def getis(geo_tus,geo_dists,ms,target_dist,var,rand=0):
    geo_list = geo_tus['GEO'].tolist()
    #fam = csa_tus['family_ratio'].tolist()
    fam = geo_tus[var].tolist()
    if rand == 1:
        fam = rand_array(fam)
    elif rand == 2:
        fam = [random.random() for i in fam]
    geo_ref = geo_dists.columns.tolist()
    weighted_sum = 0
    unweighted_sum = 0
    weights = 0
    weights_array = []
    #target_dist = 1000
    for i in range(len(geo_list)):
        temp_weights = 0
        for j in range(len(geo_list)):
            if i!=j:
                unweighted_sum += fam[i]*fam[j]
                if geo_dists.iloc[geo_ref.index(geo_list[i])][geo_list[j]] <= target_dist:
                    weighted_sum += fam[i]*fam[j]
                    weights += 1
                    temp_weights += 1
        weights_array.append(temp_weights)
                        
    Gd = weighted_sum/unweighted_sum
    EGd = weights / (len(fam)*(len(fam)-1))
    S1 = weights *2
    S2 = sum([(i*2)**2 for i in weights_array])
    n = len(fam)
    n2 = nr(n,2)
    #n3 = nr(n,3)
    n4 = nr(n,4)
    B0 = (n2 - 3*n + 3) * S1 - n*S2 + 3*weights**2
    B1 = -((n2-n)*S1-2*n*S2+3*weights**2)
    B2 = -(2*n*S1 - (n+3)*S2 + 6*weights**2)
    B3 = 4*(n-1)*S1-2*(n+1)*S2+8*weights**2
    B4 = S1-S2+weights**2
    
    EG2 = (1/((ms['m1']**2 - ms['m2'])**2 * n4)) *(B0*ms['m2']**2 +\
                                                 B1*ms['m4']+\
                                                 B2*ms['m1']**2*ms['m2']+\
                                                 B3*ms['m1']*ms['m3']+\
                                                 B4*ms['m1']**4)

    #diffs.append(Gd-EGd)
    
    var_g = EG2 - (weights / n2)**2
    z = (Gd-EGd) / var_g**0.5
    #print(Gd,EGd,var_g,z)
    #print(S1,S2)'
    #print(B0,B1,B2,B3,B4)
    #print(ms)
    #print((1/(ms['m1']**2 - ms['m2'])**2 * n4))
    #print(weights)
    return(z)
#%% Loop Params for G

#var_list = ['perc_native','family_ratio','ts_coworker','ts_nh_family','ts_friends',]
var_list = ['ts_nh_family']

for var in var_list:
    geo_tus = csa_tus
    geo_dists = csa_dists
    target_dist = 5000
    ms = {'m1': sum([i for i in geo_tus[var].tolist()]),
            'm2': sum([i**2 for i in geo_tus[var].tolist()]),
            'm3': sum([i**3 for i in geo_tus[var].tolist()]),
            'm4': sum([i**4 for i in geo_tus[var].tolist()])}
    
    z_real_hist = []
    z_rand_hist = []
    for i in range(990,995,5):
        print(i)
        target_dist = i
        z = getis(geo_tus,geo_dists,ms,target_dist,var,rand=0)
        z_real_hist.append(z)
        temp_z = []
        for j in range(10):
            z = getis(geo_tus,geo_dists,ms,target_dist,var,rand=1)
            temp_z.append(z)
        z_rand_hist.append(sum(temp_z)/len(temp_z))
    
    x = [i for i in range(990,995,5)]
    
    plt.plot(x,z_real_hist)
    plt.plot(x,z_rand_hist)
    plt.title('CSA - '+var)
    plt.show()

#%% Loop Params for G (MSA)

var_list = ['ts_nh_family']

for var in var_list:
    geo_tus = msa_tus
    geo_dists = msa_dists
    ms = {'m1': sum([i for i in geo_tus[var].tolist()]),
            'm2': sum([i**2 for i in geo_tus[var].tolist()]),
            'm3': sum([i**3 for i in geo_tus[var].tolist()]),
            'm4': sum([i**4 for i in geo_tus[var].tolist()])}
        
    z_real_hist = []
    z_rand_hist = []
    for i in range(370,380,10):
        print(i)
        target_dist = i
        z = getis(geo_tus,geo_dists,ms,target_dist,var,rand=0)
        z_real_hist.append(z)
        temp_z = []
        for j in range(10):
            z = getis(geo_tus,geo_dists,ms,target_dist,var,rand=1)
            temp_z.append(z)
        z_rand_hist.append(sum(temp_z)/len(temp_z))
    
    x = [i for i in range(370,380,10)]
    
    plt.plot(x,z_real_hist)
    plt.plot(x,z_rand_hist)
    plt.title('MSA - '+var)
    plt.show()
#%%

x = [i for i in range(100,600,50)]

plt.plot(x,z_real_hist)
plt.plot(x,z_rand_hist)
plt.title('MSA - '+var)
plt.show()
#%% G_i Statistic

def calc_gi(data,dists,target_dist,param):
    GiD = []
    EGi = []
    geo_list = data['GEO'].tolist()
    #fam = data['family_ratio'].tolist()
    #fam = data['perc_native'].tolist()
    fam = data[param].tolist()
    geo_ref = dists.columns.tolist()
    var = []
    zs = []
    
    for i in range(len(geo_list)):
        w_sum = 0
        uw_sum = 0
        w_count = 0
        xj = 0
        xjsq = 0
        for j in range(len(geo_list)):
            uw_sum += fam[j]#fam[i]*fam[j]
            xj += fam[j]
            xjsq += fam[j]**2
            if dists.iloc[geo_ref.index(geo_list[i])][geo_list[j]] <= target_dist:
                w_sum += fam[j]# fam[i]*fam[j]
                w_count += 1
        GiD.append(w_sum/uw_sum)
        EGi.append(w_count/(len(geo_list)-1))
        y1 = xj / (len(geo_list)-1)
        y2 = xjsq / (len(geo_list)-1) - y1**2
        var = w_count * (len(geo_list) - 1 - w_count) / ((len(geo_list)-1)**2 * (len(geo_list)-2)) * (y2/y1**2)
        try:
            z = (GiD[-1]-EGi[-1])/var**0.5
        except:
            z = np.nan
        zs.append(z)
    #print(zs)
    #plt.hist(zs)
    #plt.show()
    return(GiD,EGi,zs)

def calc_rand_gi(data,dists,target_dist,param):
    GiD = []
    EGi = []
    geo_list = data['GEO'].tolist()
    #fam = data['family_ratio'].tolist()
    #fam = data['perc_native'].tolist()
    fam = data[param].tolist()
    geo_ref = dists.columns.tolist()
    var = []
    zs = []
    
    fam = rand_array(fam)
    
    for i in range(len(geo_list)):
        w_sum = 0
        uw_sum = 0
        w_count = 0
        xj = 0
        xjsq = 0
        for j in range(len(geo_list)):
            uw_sum += fam[j]#fam[i]*fam[j]
            xj += fam[j]
            xjsq += fam[j]**2
            if dists.iloc[geo_ref.index(geo_list[i])][geo_list[j]] <= target_dist:
                w_sum += fam[j]# fam[i]*fam[j]
                w_count += 1
        GiD.append(w_sum/uw_sum)
        EGi.append(w_count/(len(geo_list)-1))
        y1 = xj / (len(geo_list)-1)
        y2 = xjsq / (len(geo_list)-1) - y1**2
        var = w_count * (len(geo_list) - 1 - w_count) / ((len(geo_list)-1)**2 * (len(geo_list)-2)) * (y2/y1**2)
        try:
            z = (GiD[-1]-EGi[-1])/var**0.5
        except:
            z = np.nan
        zs.append(z)
    #print(zs)
    #plt.hist(zs)
    #plt.show()
    return(GiD,EGi,zs)

def above_below(a,b):
    a_greater = []
    for i in range(len(a)):
        if a[i] > b[i]:
            a_greater.append(1)
        else:
            a_greater.append(0)
    return(a_greater)

# zs = []
# zrss = []
# ds = []

# for d in range(100,3000,100):
#     print(d)
#     ds.append(d)
#     g,e,z = calc_gi(csa_tus,csa_dists,d,'ts_nh_family')
    
#     zr_s = []
#     for x in range(10):
#         gr,er,zr = calc_rand_gi(csa_tus,csa_dists,d)
#         zr_s += zr
#     zs.append(z)
#     zrss.append(zr_s)
    #plt.hist(z,density=True,alpha=0.6,bins=20)
    #plt.hist(zr_s,alpha=0.6,density=True,bins=20)

#gf = [g[i] for i in range(len(g)) if g[i]+e[i]>0]
#ef = [e[i] for i in range(len(e)) if g[i]+e[i]>0]
#x = [(i/(1/max(g))/100) for i in range(100)]
#y = x

#colors = ['green' if gf[i] > ef[i] else 'darkorange' for i in range(len(gf))]
#plt.scatter(gf,ef,s=10,c=colors,alpha=0.6)
#plt.yscale('log')
#plt.xscale('log')
#plt.show()

#%% G_i for KNN

def calc_gi_kth(data,dists,param):
    GiD = []
    EGi = []
    geo_list = data['GEO'].tolist()
    dist_list = data['K_dist'].tolist()
    #fam = data['family_ratio'].tolist()
    #fam = data['perc_native'].tolist()
    fam = data[param].tolist()
    geo_ref = dists.columns.tolist()
    var = []
    zs = []
    
    for i in range(len(geo_list)):
        w_sum = 0
        uw_sum = 0
        w_count = 0
        xj = 0
        xjsq = 0
        target_dist = dist_list[i]
        for j in range(len(geo_list)):
            uw_sum += fam[j]#fam[i]*fam[j]
            xj += fam[j]
            xjsq += fam[j]**2
            if dists.iloc[geo_ref.index(geo_list[i])][geo_list[j]] <= target_dist:
                w_sum += fam[j]# fam[i]*fam[j]
                w_count += 1
        GiD.append(w_sum/uw_sum)
        EGi.append(w_count/(len(geo_list)-1))
        y1 = xj / (len(geo_list)-1)
        y2 = xjsq / (len(geo_list)-1) - y1**2
        var = w_count * (len(geo_list) - 1 - w_count) / ((len(geo_list)-1)**2 * (len(geo_list)-2)) * (y2/y1**2)
        try:
            z = (GiD[-1]-EGi[-1])/var**0.5
        except:
            z = np.nan
        zs.append(z)
    #print(zs)
    #plt.hist(zs)
    #plt.show()
    return(GiD,EGi,zs)


def calc_rand_gi_kth(data,dists,param):
    GiD = []
    EGi = []
    geo_list = data['GEO'].tolist()
    dist_list = data['K_dist'].tolist()
    #fam = data['family_ratio'].tolist()
    #fam = data['perc_native'].tolist()
    fam = data[param].tolist()
    geo_ref = dists.columns.tolist()
    var = []
    zs = []
    
    fam = rand_array(fam)
    
    for i in range(len(geo_list)):
        w_sum = 0
        uw_sum = 0
        w_count = 0
        xj = 0
        xjsq = 0
        target_dist = dist_list[i]
        for j in range(len(geo_list)):
            uw_sum += fam[j]#fam[i]*fam[j]
            xj += fam[j]
            xjsq += fam[j]**2
            if dists.iloc[geo_ref.index(geo_list[i])][geo_list[j]] <= target_dist:
                w_sum += fam[j]# fam[i]*fam[j]
                w_count += 1
        GiD.append(w_sum/uw_sum)
        EGi.append(w_count/(len(geo_list)-1))
        y1 = xj / (len(geo_list)-1)
        y2 = xjsq / (len(geo_list)-1) - y1**2
        var = w_count * (len(geo_list) - 1 - w_count) / ((len(geo_list)-1)**2 * (len(geo_list)-2)) * (y2/y1**2)
        try:
            z = (GiD[-1]-EGi[-1])/var**0.5
        except:
            z = np.nan
        zs.append(z)
    #print(zs)
    #plt.hist(zs)
    #plt.show()
    return(GiD,EGi,zs)

#%% CSA G_i param loop

#var_strings = ['ts_nh_family','family_ratio']
var_strings = ['ts_nh_family']

for var_s in var_strings:
    zs = []
    zrss = []
    ds = []
    
    geos = csa_tus['GEO'].tolist()
    print(geos)
    
    for d in range(990,1000,100):
        print(d)
        ds.append(d)
        g,e,z = calc_gi(csa_tus,csa_dists,d,var_s)
        
        zr_s = []
        for x in range(20):
            gr,er,zr = calc_rand_gi(csa_tus,csa_dists,d,var_s)
            zr_s += zr
        zs.append(z)
        zrss.append(zr_s)
        
    
    # for geo in range(len(zs[0])):
    #     z = [i[geo] for i in zs]
    #     zr = [i[geo] for i in zrss]
    #     plt.plot(ds,z)
    #     plt.plot(ds,zr)
    #     plt.title(geos[geo]+" - "+var_s)
    #     plt.show()
    
    # for geo in range(len(zs[0])):
    #     z = [i[geo] for i in zs]
    #     #zr = [i[geo] for i in zrss]
    #     plt.plot(ds,z,alpha=0.6)
    #     #plt.plot(ds,zr)
    # plt.title("All Geos - "+var_s)
    # plt.show()

#%%

temp_df = pd.DataFrame(data={'GEO':geos,'Zs':zs[0],'ts_nh_family':csa_tus['ts_nh_family']})
temp_df.to_csv('csa_tsfam_gi_stat.csv')


#%% MSA G_i param loop


#var_strings = ['ts_nh_family','family_ratio']
var_strings = ['ts_nh_family']

for var_s in var_strings:
    zs = []
    zrss = []
    ds = []
    
    geos = msa_tus['GEO'].tolist()
    print(geos)
    
    for d in range(370,380,10):
        print(d)
        ds.append(d)
        g,e,z = calc_gi(msa_tus,msa_dists,d,var_s)
        
        zr_s = []
        for x in range(20):
            gr,er,zr = calc_rand_gi(msa_tus,msa_dists,d,var_s)
            zr_s += zr
        zs.append(z)
        zrss.append(zr_s)
        
    
    # for geo in range(len(zs[0])):
    #     z = [i[geo] for i in zs]
    #     zr = [i[geo] for i in zrss]
    #     plt.plot(ds,z)
    #     plt.plot(ds,zr)
    #     plt.title(geos[i]+" - "+var_s)
    #     plt.show()
    
    # for geo in range(len(zs[0])):
    #     z = [i[geo] for i in zs]
    #     #zr = [i[geo] for i in zrss]
    #     plt.plot(ds,z,alpha=0.6)
    #     #plt.plot(ds,zr)
    # plt.title("All Geos - "+var_s)
    # plt.show()
    

temp_df = pd.DataFrame(data={'GEO':geos,'Zs':zs[0],'ts_nh_family':msa_tus['ts_nh_family']})
temp_df.to_csv('msa_tsfam_gi_stat.csv')
    
#%%
for geo in range(len(zs[0])):
    z = [i[geo] for i in zs]
    zr = [i[geo] for i in zrss]
    plt.plot(ds,z)
    plt.plot(ds,zr)
    plt.title(geos[geo]+" - "+var_s)
    plt.show()

for geo in range(len(zs[0])):
    z = [i[geo] for i in zs]
    #zr = [i[geo] for i in zrss]
    plt.plot(ds,z,alpha=0.6)
    #plt.plot(ds,zr)
plt.title("All Geos - "+var_s)
plt.show()
    
#%% KNN param loops

def kth_n_dist(geo_df,dist_df,k):
    geo_array = geo_df['GEO'].tolist()
    geo_index = dist_df.index.tolist()
    dist = []
    for g in geo_array:
        if g in geo_index:
            row = sorted(dist_df.iloc[geo_index.index(g)].replace(np.nan,1000000).tolist())
            d = row[k-1]
            dist.append(d)
        else:
            dist.append(0)
    out_df = copy.deepcopy(geo_df)
    out_df['K_dist'] = dist
    return(out_df)

zs_hist = []
zr_hist = []
x = []
for i in range(2,11):
    x.append(i)
    csa_tus_k = kth_n_dist(csa_tus,csa_dists,i)
    GiD,EGi,zs = calc_gi_kth(csa_tus_k,csa_dists,'ts_nh_family')
    zs_hist.append(zs)
    
    zr_temp = []
    for j in range(10):
        GiD,EGi,zr = calc_rand_gi_kth(csa_tus_k,csa_dists,'ts_nh_family')
        zr_temp.append(zr)
    zr_current = []
    for j in range(len(zr_temp[0])):
        zr_current.append(sum([zr_temp[k][j] for k in range(len(zr_temp))])/10)
    zr_hist.append(zr_current)
    
    
#%% Plots of KNN param loops

for i in range(len(zs_hist[0])):
    plt.plot(x,[zs_hist[j][i] for j in range(len(zs_hist))])
    plt.plot(x,[zr_hist[j][i] for j in range(len(zr_hist))],alpha=0.5)
    plt.show()

for i in range(len(zs_hist)):
    plt.hist(zs_hist[i],alpha=0.6,density=True)
    plt.hist(zr_hist[i],alpha=0.6,density=True)
    plt.show()
    
    
#%% TO DO LIST

# TODO: G statistic w/ region instead of distance
# TODO: get plotly working
# DONE: implement equations 2 and 3 for G_i  (4 is the actual z score- do this too)
# DONE: implement equations 6, 7, and 7 for G
# TODO: look up Moran Coefficient



#%% Region checker function

def region_checker(a,b,regions):
    a_regions = regions[a]
    b_regions = regions[b]
    shared = False
    for r in a_regions:
        if r in b_regions:
            shared = True
    return(shared)

#%% Regional G

def getis(geo_tus,regions,ms,var,rand=0):
    
    geo_list = geo_tus['GEO'].tolist()
    #fam = csa_tus['family_ratio'].tolist()
    fam = geo_tus[var].tolist()
    if rand == 1:
        fam = rand_array(fam)
    elif rand == 2:
        fam = [random.random() for i in fam]

    weighted_sum = 0
    unweighted_sum = 0
    weights = 0
    weights_array = []
    #target_dist = 1000
    for i in range(len(geo_list)):
        temp_weights = 0
        for j in range(len(geo_list)):
            if i!=j:
                unweighted_sum += fam[i]*fam[j]
                if region_checker(geo_list[i],geo_list[j],regions):
                    weighted_sum += fam[i]*fam[j]
                    weights += 1
                    temp_weights += 1
        weights_array.append(temp_weights)
                        
    Gd = weighted_sum/unweighted_sum
    EGd = weights / (len(fam)*(len(fam)-1))
    S1 = weights *2
    S2 = sum([(i*2)**2 for i in weights_array])
    n = len(fam)
    n2 = nr(n,2)
    #n3 = nr(n,3)
    n4 = nr(n,4)
    B0 = (n2 - 3*n + 3) * S1 - n*S2 + 3*weights**2
    B1 = -((n2-n)*S1-2*n*S2+3*weights**2)
    B2 = -(2*n*S1 - (n+3)*S2 + 6*weights**2)
    B3 = 4*(n-1)*S1-2*(n+1)*S2+8*weights**2
    B4 = S1-S2+weights**2
    
    EG2 = (1/((ms['m1']**2 - ms['m2'])**2 * n4)) *(B0*ms['m2']**2 +\
                                                 B1*ms['m4']+\
                                                 B2*ms['m1']**2*ms['m2']+\
                                                 B3*ms['m1']*ms['m3']+\
                                                 B4*ms['m1']**4)
    
    var_g = EG2 - (weights / n2)**2
    z = (Gd-EGd) / var_g**0.5
    print(Gd,EGd,z)
    return(z)


var = 'ts_nh_family'
ms = {'m1': sum([i for i in csa_tus[var].tolist()]),
        'm2': sum([i**2 for i in csa_tus[var].tolist()]),
        'm3': sum([i**3 for i in csa_tus[var].tolist()]),
        'm4': sum([i**4 for i in csa_tus[var].tolist()])}

z = getis(csa_tus,csa_regions,ms,var,rand=0)


#%% Regional G_i


def calc_gi(data,regions,param):
    GiD = []
    EGi = []
    geo_list = data['GEO'].tolist()
    print(geo_list)
    #fam = data['family_ratio'].tolist()
    #fam = data['perc_native'].tolist()
    fam = data[param].tolist()

    var = []
    zs = []
    
    for i in range(len(geo_list)):
        w_sum = 0
        uw_sum = 0
        w_count = 0
        xj = 0
        xjsq = 0
        for j in range(len(geo_list)):
            uw_sum += fam[j]#fam[i]*fam[j]
            xj += fam[j]
            xjsq += fam[j]**2
            
            if region_checker(geo_list[i],geo_list[j],regions):
            
            #if dists.iloc[geo_ref.index(geo_list[i])][geo_list[j]] <= target_dist:
                w_sum += fam[j]# fam[i]*fam[j]
                w_count += 1
        GiD.append(w_sum/uw_sum)
        EGi.append(w_count/(len(geo_list)-1))
        y1 = xj / (len(geo_list)-1)
        y2 = xjsq / (len(geo_list)-1) - y1**2
        var = w_count * (len(geo_list) - 1 - w_count) / ((len(geo_list)-1)**2 * (len(geo_list)-2)) * (y2/y1**2)
        try:
            z = (GiD[-1]-EGi[-1])/var**0.5
        except:
            z = np.nan
        zs.append(z)
    #print(zs)
    #plt.hist(zs)
    #plt.show()
    return(GiD,EGi,zs,geo_list)

GiD,EGi,zs,geos = calc_gi(csa_tus,csa_regions,'ts_nh_family')

#GiD,EGi,zs,geos = calc_gi(msa_tus,msa_regions,'perc_native')

#%%

for i in range(len(zs)):
    print(geos[i],csa_regions[geos[i]],zs[i])
    
#%%

def reverse_dict(d_in):
    d_out = {}
    for k,v in d_in.items():
        for i in v:
            if i in d_out:
                d_out[i].append(k)
            else:
                d_out[i] = [k]
    return(d_out)

regions_csa = reverse_dict(csa_regions)
regions_msa = reverse_dict(msa_regions)


def plot_region_stuff(df,rd,dr):
    pop = []
    nat = []
    tsf = []
    den = []
    reg = []
    for k,v in rd.items():
        #print(k)
        temp_df = df[df['GEO'].isin(v)]
        print(temp_df.head())
        fig,axs = plt.subplots(2,2,sharex='col',sharey='row')
        axs[0,0].scatter(temp_df['census_pop'].tolist(),temp_df['perc_native'].tolist())
        axs[0,0].set_ylabel('Perc Native')
        axs[1,0].scatter(temp_df['census_pop'].tolist(),temp_df['ts_nh_family'].tolist())
        axs[1,0].set_xlabel('Census Pop')
        axs[1,0].set_ylabel('Time Spent w Family')
        axs[0,1].scatter(temp_df['Pop_Density'].tolist(),temp_df['perc_native'].tolist())
        axs[1,1].scatter(temp_df['Pop_Density'].tolist(),temp_df['ts_nh_family'].tolist())
        axs[1,1].set_xlabel('Pop Density')
        axs[0,0].set_xscale('log')
        axs[0,1].set_xscale('log')
        fig.suptitle(k)
        plt.tight_layout()
        plt.show()
        
        pop += temp_df['census_pop'].tolist()
        nat += temp_df['perc_native'].tolist()
        tsf += temp_df['ts_nh_family'].tolist()
        den += temp_df['Pop_Density'].tolist()
        reg += [k]*len(temp_df)
    fig,axs = plt.subplots(2,2,sharex='col',sharey='row',figsize=(10,6))
    for r in set(reg):
        print(r)
        axs[0,0].scatter([pop[i] for i in range(len(pop)) if reg[i]==r],\
                         [nat[i] for i in range(len(nat)) if reg[i]==r],\
                         label=r,s=20,alpha=0.8)
        axs[0,0].set_ylabel('Perc Native')
        axs[1,0].scatter([pop[i] for i in range(len(pop)) if reg[i]==r],\
                         [tsf[i] for i in range(len(tsf)) if reg[i]==r],\
                          s=20,alpha=0.8)
        axs[1,0].set_xlabel('Census Pop')
        axs[1,0].set_ylabel('Time Spent w Family')
        axs[0,1].scatter([den[i] for i in range(len(den)) if reg[i]==r],\
                         [nat[i] for i in range(len(nat)) if reg[i]==r],\
                          s=20,alpha=0.8)
        axs[1,1].scatter([den[i] for i in range(len(den)) if reg[i]==r],\
                         [tsf[i] for i in range(len(tsf)) if reg[i]==r],\
                         s=20,alpha=0.8)
        axs[1,1].set_xlabel('Pop Density')
        axs[0,0].set_xscale('log')
        axs[0,1].set_xscale('log')
    fig.suptitle(k)
    axs[0,0].legend(bbox_to_anchor=(-0.3,1))
    plt.tight_layout()
    plt.show()
    

plot_region_stuff(csa_tus,regions_csa,csa_regions)
plot_region_stuff(msa_tus,regions_msa,msa_regions)

#%%

def region_corrs(df,rd,dr):
    output = [['Region','Pop & Perc_Nat', 'Pop & TS_Fam','Dens & Perc_Nat', 'Dens & TS_Fam']]
    output_p = [['Region','Pop & Perc_Nat', 'Pop & TS_Fam','Dens & Perc_Nat', 'Dens & TS_Fam']]
    pop = []
    nat = []
    tsf = []
    den = []
    reg = []

    for k,v in rd.items():
        
        row = [k]
        row_p = [k]
        
        temp_df = df[df['GEO'].isin(v)]
        
        for i in ['census_pop', 'Pop_Density']:
            for j in ['perc_native','ts_nh_family']:
                pc = stats.pearsonr(temp_df[i].tolist(),temp_df[j].tolist())
                row.append(pc[0])
                row_p.append(pc[1])

   
        pop += temp_df['census_pop'].tolist()
        nat += temp_df['perc_native'].tolist()
        tsf += temp_df['ts_nh_family'].tolist()
        den += temp_df['Pop_Density'].tolist()
        reg += [k]*len(temp_df)
        output.append(row)
        output_p.append(row_p)
        
    
    
    pc1 = stats.pearsonr(pop,nat)
    pc2 = stats.pearsonr(pop,tsf)
    pc3 = stats.pearsonr(den,nat)
    pc4 = stats.pearsonr(den,tsf)
    output.append(['ALL',pc1[0],pc2[0],pc3[0],pc4[0]])
    output_p.append(['ALL',pc1[1],pc2[1],pc3[1],pc4[1]])
    
    for i in output: print(i)
    print()
    for i in output_p: print(i)
    print()
    
    
region_corrs(csa_tus,regions_csa,csa_regions)
region_corrs(msa_tus,regions_msa,msa_regions)

#%% Plot and Correlate

def plot_corr(df,rd,dr,s):
    pop = []
    nat = []
    tsf = []
    den = []
    reg = []
    for k,v in rd.items():
        #print(k)
        temp_df = df[df['GEO'].isin(v)]
        
        row = []
        
        for i in ['census_pop', 'Pop_Density']:
            for j in ['perc_native','ts_nh_family']:
                pc = stats.pearsonr(temp_df[i].tolist(),temp_df[j].tolist())
                row.append(pc[0])
        
        print(temp_df.head())
        fig,axs = plt.subplots(2,2,sharex='col',sharey='row',figsize=(6,6))
        axs[0,0].scatter(temp_df['census_pop'].tolist(),temp_df['perc_native'].tolist())
        axs[0,0].set_ylabel('Perc Native')
        axs[0,0].set_title('Corr = '+str(row[0])[0:6])
        axs[1,0].scatter(temp_df['census_pop'].tolist(),temp_df['ts_nh_family'].tolist())
        axs[1,0].set_xlabel('Census Pop')
        axs[1,0].set_ylabel('Time Spent w Family')
        axs[1,0].set_title('Corr = '+str(row[1])[0:6])
        axs[0,1].scatter(temp_df['Pop_Density'].tolist(),temp_df['perc_native'].tolist())
        axs[0,1].set_title('Corr = '+str(row[2])[0:6])
        axs[1,1].scatter(temp_df['Pop_Density'].tolist(),temp_df['ts_nh_family'].tolist())
        axs[1,1].set_title('Corr = '+str(row[3])[0:6])
        axs[1,1].set_xlabel('Pop Density')
        axs[0,0].set_xscale('log')
        #axs[0,1].set_xscale('log')
        fig.suptitle(s+' - '+k)
        plt.tight_layout()
        plt.show()
        
        pop += temp_df['census_pop'].tolist()
        nat += temp_df['perc_native'].tolist()
        tsf += temp_df['ts_nh_family'].tolist()
        den += temp_df['Pop_Density'].tolist()
        reg += [k]*len(temp_df)
    fig,axs = plt.subplots(2,2,sharex='col',sharey='row',figsize=(8,6))
    
    pc1 = stats.pearsonr(pop,nat)
    pc2 = stats.pearsonr(pop,tsf)
    pc3 = stats.pearsonr(den,nat)
    pc4 = stats.pearsonr(den,tsf)
    for r in set(reg):
        print(r)
        axs[0,0].scatter([pop[i] for i in range(len(pop)) if reg[i]==r],\
                         [nat[i] for i in range(len(nat)) if reg[i]==r],\
                         label=r,s=20,alpha=0.8)
        axs[0,0].set_ylabel('Perc Native')
        axs[0,0].set_title('Corr = '+str(pc1[0])[0:6])
        axs[1,0].scatter([pop[i] for i in range(len(pop)) if reg[i]==r],\
                         [tsf[i] for i in range(len(tsf)) if reg[i]==r],\
                          s=20,alpha=0.8)
        axs[1,0].set_xlabel('Census Pop')
        axs[1,0].set_title('Corr = '+str(pc2[0])[0:6])
        axs[1,0].set_ylabel('Time Spent w Family')
        axs[0,1].scatter([den[i] for i in range(len(den)) if reg[i]==r],\
                         [nat[i] for i in range(len(nat)) if reg[i]==r],\
                          s=20,alpha=0.8)
        axs[0,1].set_title('Corr = '+str(pc3[0])[0:6])
        axs[1,1].scatter([den[i] for i in range(len(den)) if reg[i]==r],\
                         [tsf[i] for i in range(len(tsf)) if reg[i]==r],\
                         s=20,alpha=0.8)
        axs[1,1].set_xlabel('Pop Density')
        axs[1,1].set_title('Corr = '+str(pc4[0])[0:6])
        axs[0,0].set_xscale('log')
        #axs[0,1].set_xscale('log')
    fig.suptitle(s+ ' - '+k)
    axs[0,0].legend(bbox_to_anchor=(-0.3,1))
    plt.tight_layout()
    plt.show()
    

plot_corr(csa_tus,regions_csa,csa_regions,'CSA')
plot_corr(msa_tus,regions_msa,msa_regions,'MSA')


#%%

def convert_dists_tus(t_in,d_in):
    dists = copy.deepcopy(d_in)
    tus = copy.deepcopy(t_in)
    geos = tus['GEO'].tolist()
    for g in dists.columns.tolist():
        if g not in geos:
            dists = dists.drop([g],axis=0)
            dists = dists.drop([g],axis=1)
    geos = dists.columns.tolist()
    rows = []
    for row in tus.iterrows():
        if row[1]['GEO'] not in geos:
            rows.append(row[0])
    for i in rows:
        tus = tus.drop([i])
    return dists,tus
    
csa_dists_tus, csa_tus_trunc = convert_dists_tus(csa_tus,csa_dists)
msa_dists_tus, msa_tus_trunc = convert_dists_tus(msa_tus,msa_dists)

#%% G Stat KNN

def getis_knn(geo_tus,geo_dists,ms,target_dist,var,rand=0):
    geo_list = geo_tus['GEO'].tolist()
    fam = geo_tus[var].tolist()
    if rand == 1:
        fam = rand_array(fam)
    elif rand == 2:
        fam = [random.random() for i in fam]
    geo_ref = geo_dists.columns.tolist()
    weighted_sum = 0
    unweighted_sum = 0
    weights = 0
    weights_array = []
    for i in range(len(geo_list)):
        temp_weights = 0
        dist_array = [i if i > 0 else 999999 for i in geo_dists.iloc[geo_ref.index(geo_list[i])].tolist()]
        sorted_dist_array = sorted(dist_array)
        for j in range(len(geo_list)):
            if i!=j:
                
                unweighted_sum += fam[i]*fam[j]
                if dist_array[j] <= sorted_dist_array[target_dist]:
                    weighted_sum += fam[i]*fam[j]
                    weights += 1
                    temp_weights += 1
        weights_array.append(temp_weights)
    Gd = weighted_sum/unweighted_sum
    EGd = weights / (len(fam)*(len(fam)-1))
    S1 = weights *2
    S2 = sum([(i*2)**2 for i in weights_array])
    n = len(fam)
    n2 = nr(n,2)
    #n3 = nr(n,3)
    n4 = nr(n,4)
    B0 = (n2 - 3*n + 3) * S1 - n*S2 + 3*weights**2
    B1 = -((n2-n)*S1-2*n*S2+3*weights**2)
    B2 = -(2*n*S1 - (n+3)*S2 + 6*weights**2)
    B3 = 4*(n-1)*S1-2*(n+1)*S2+8*weights**2
    B4 = S1-S2+weights**2
    
    EG2 = (1/((ms['m1']**2 - ms['m2'])**2 * n4)) *(B0*ms['m2']**2 +\
                                                 B1*ms['m4']+\
                                                 B2*ms['m1']**2*ms['m2']+\
                                                 B3*ms['m1']*ms['m3']+\
                                                 B4*ms['m1']**4)

    var_g = EG2 - (weights / n2)**2
    z = (Gd-EGd) / var_g**0.5
    print(target_dist,Gd,EG2, var_g,z)
    return(z)


#%%

def calc_gi_knn(data,dists,target_dist,param):
    GiD = []
    EGi = []
    geo_list = data['GEO'].tolist()
    fam = data[param].tolist()
    geo_ref = dists.columns.tolist()
    var = []
    zs = []
    
    for i in range(len(geo_list)):
        w_sum = 0
        uw_sum = 0
        w_count = 0
        xj = 0
        xjsq = 0
        dist_array = [i if i > 0 else 99999 for i in dists.iloc[geo_ref.index(geo_list[i])].tolist() ]
        sorted_dist_array = sorted(dist_array)
        for j in range(len(geo_list)):
            uw_sum += fam[j]#fam[i]*fam[j]
            xj += fam[j]
            xjsq += fam[j]**2
            if dist_array[geo_ref.index(geo_list[j])] < sorted_dist_array[target_dist]:
                w_sum += fam[j]# fam[i]*fam[j]
                w_count += 1
        GiD.append(w_sum/uw_sum)
        EGi.append(w_count/(len(geo_list)-1))
        y1 = xj / (len(geo_list)-1)
        y2 = xjsq / (len(geo_list)-1) - y1**2
        var = w_count * (len(geo_list) - 1 - w_count) / ((len(geo_list)-1)**2 * (len(geo_list)-2)) * (y2/y1**2)
        try:
            z = (GiD[-1]-EGi[-1])/var**0.5
        except:
            z = np.nan
        zs.append(z)
    #print(zs)
    #plt.hist(zs)
    #plt.show()
    print(zs)
    return(GiD,EGi,zs)

#%%

var = 'ts_nh_family'
ms = {'m1': sum([i for i in csa_tus[var].tolist()]),
        'm2': sum([i**2 for i in csa_tus[var].tolist()]),
        'm3': sum([i**3 for i in csa_tus[var].tolist()]),
        'm4': sum([i**4 for i in csa_tus[var].tolist()])}

for n in range(100,2000,100):
    z = getis(csa_tus,csa_dists,ms,n,var,rand=0)
    print(n,z)
print()


#%%

mi,ma = 1,len(csa_tus)
#mi,ma = 15,25


var = 'ts_nh_family'
ms = {'m1': sum([i for i in csa_tus_trunc[var].tolist()]),
        'm2': sum([i**2 for i in csa_tus_trunc[var].tolist()]),
        'm3': sum([i**3 for i in csa_tus_trunc[var].tolist()]),
        'm4': sum([i**4 for i in csa_tus_trunc[var].tolist()])}

zs = []
for n in range(mi,ma):
    z = getis_knn(csa_tus_trunc,csa_dists_tus,ms,n,var,rand=0)
    zs.append(z)
    #print(n,z)
#print()

plt.plot([i for i in range(len(zs))],zs)

zs = []
for n in range(mi,ma):
    z_sum = 0
    for i in range(20):
        z_sum += getis_knn(csa_tus_trunc,csa_dists_tus,ms,n,var,rand=1)
    z_sum /= 20
    zs.append(z_sum)
    
plt.plot([i for i in range(len(zs))],zs)

#%%
ms = {'m1': sum([i for i in msa_tus_trunc[var].tolist()]),
        'm2': sum([i**2 for i in msa_tus_trunc[var].tolist()]),
        'm3': sum([i**3 for i in msa_tus_trunc[var].tolist()]),
        'm4': sum([i**4 for i in msa_tus_trunc[var].tolist()])}

zs = []
for n in range(10,25):#range(1,len(msa_tus)):   
    z = getis_knn(msa_tus_trunc,msa_dists_tus,ms,n,var,rand=0)
    zs.append(z)
    print(n,z)
plt.plot([i for i in range(len(zs))],zs)
#%%

GiD,EGi,zs = calc_gi_knn(csa_tus,csa_dists_tus,10,'ts_nh_family')
plt.hist(zs)

csa_trunc_list = csa_tus_trunc['GEO'].tolist()
for i in range(len(csa_trunc_list)):
    print(csa_trunc_list[i],'#',zs[i])

#%%

GiD,EGi,zs = calc_gi_knn(msa_tus_trunc,msa_dists_tus,16,'ts_nh_family')
plt.hist(zs)


msa_trunc_list = msa_tus_trunc['GEO'].tolist()
for i in range(len(msa_trunc_list)):
    print(msa_trunc_list[i]+'#',zs[i])

#%%

'''REDO with Perc Native on non-truncated data'''

csa_df = csa_df[csa_df['perc_native'].notna()]
msa_df = msa_df[msa_df['perc_native'].notna()]

csa_dists_trunc, csa_df_trunc = convert_dists_tus(csa_df,csa_dists)
msa_dists_trunc, msa_df_trunc = convert_dists_tus(msa_df,msa_dists)

#csa_df_trunc['perc_native'] = csa_df_trunc['perc_native'] * 100
#msa_df_trunc['perc_native'] = msa_df_trunc['perc_native'] * 100

#%%

mi,ma = 1,len(csa_df_trunc)
#mi,ma = 15,25


var = 'perc_native'
ms = {'m1': sum([i for i in csa_df_trunc[var].tolist()]),
        'm2': sum([i**2 for i in csa_df_trunc[var].tolist()]),
        'm3': sum([i**3 for i in csa_df_trunc[var].tolist()]),
        'm4': sum([i**4 for i in csa_df_trunc[var].tolist()])}

zs = []
for n in range(mi,ma):
    z = getis_knn(csa_df_trunc,csa_dists_trunc,ms,n,var,rand=0)
    zs.append(z)
    #print(n,z)
#print()

plt.plot([i for i in range(len(zs))],zs)


#%%


ms = {'m1': sum([i for i in msa_df_trunc[var].tolist()]),
        'm2': sum([i**2 for i in msa_df_trunc[var].tolist()]),
        'm3': sum([i**3 for i in msa_df_trunc[var].tolist()]),
        'm4': sum([i**4 for i in msa_df_trunc[var].tolist()])}

zs = []
for n in range(1,len(msa_tus)):   
    z = getis_knn(msa_df_trunc,msa_dists_trunc,ms,n,var,rand=0)
    zs.append(z)
    print(n,z)
plt.plot([i for i in range(len(zs))],zs)


#%% 

# TODO: send MSA and CSA G_i for perc_native & ts_nh_family
# TODO: for the CSAs/MSAs with BOTH variables, do correlation + scatterplots of the two variables & associated G_i scores

#%%

# CSA: k=10
for i in range(10,11):
    GiD,EGi,zs = calc_gi_knn(csa_df_trunc,csa_dists_trunc,i,'perc_native')
    plt.hist(zs)
    plt.title('KNN for K='+str(i))
    plt.show()

csa_pn_df = csa_df_trunc[['GEO','perc_native','census_pop','ts_nh_family','Pop_Density']].copy()
csa_pn_df['perc_native_Zs'] = zs

pd.plotting.scatter_matrix(csa_pn_df,diagonal='kde',figsize=(8,8))

#csa_pn_df.to_csv('csa_percnative_gi_stat.csv')

#%%

# MSA: k=8
for i in range(8,9):
    GiD,EGi,zs = calc_gi_knn(msa_df_trunc,msa_dists_trunc,i,'perc_native')
    plt.hist(zs)
    plt.title('KNN for K='+str(i))
    plt.show()

msa_pn_df = msa_df_trunc[['GEO','perc_native','census_pop','ts_nh_family','Pop_Density']].copy()
msa_pn_df['perc_native_Zs'] = zs


pd.plotting.scatter_matrix(msa_pn_df,diagonal='kde',figsize=(8,8))

#msa_pn_df.to_csv('msa_percnative_gi_stat.csv')

#%%

# TODO: do a loop on KNN where K=1 through 10 and plot maps in a loop
# TODO: work with Unchitta 
# TODO: ask Laura (ERI) for relevant white papers and literature relevant to OPA work


#%% Percnative KNN export

csa_pn_df = csa_df_trunc[['GEO','perc_native','census_pop','ts_nh_family','Pop_Density']].copy()
# CSA: k=10
for i in range(1,15):
    GiD,EGi,zs = calc_gi_knn(csa_df_trunc,csa_dists_trunc,i,'perc_native')
    plt.hist(zs)
    plt.title('KNN for K='+str(i))
    plt.show()
    csa_pn_df['perc_native_Zs_k='+str(i)] = zs

pd.plotting.scatter_matrix(csa_pn_df,diagonal='kde',figsize=(8,8))

csa_pn_df.to_csv('csa_percnative_gi_stat.csv')

msa_pn_df = msa_df_trunc[['GEO','perc_native','census_pop','ts_nh_family','Pop_Density']].copy()
# MSA: k=8
for i in range(1,15):
    GiD,EGi,zs = calc_gi_knn(msa_df_trunc,msa_dists_trunc,i,'perc_native')
    plt.hist(zs)
    plt.title('KNN for K='+str(i))
    plt.show()
    msa_pn_df['perc_native_Zs_k='+str(i)] = zs


pd.plotting.scatter_matrix(msa_pn_df,diagonal='kde',figsize=(8,8))

msa_pn_df.to_csv('msa_percnative_gi_stat.csv')


#%% TS Family KNN export

csa_pn_df = csa_df_trunc[['GEO','perc_native','census_pop','ts_nh_family','Pop_Density']].copy()
# CSA: k=10
for i in range(1,15):
    GiD,EGi,zs = calc_gi_knn(csa_df_trunc,csa_dists_trunc,i,'ts_nh_family')
    plt.hist(zs)
    plt.title('KNN for K='+str(i))
    plt.show()
    csa_pn_df['ts_nh_family_Zs_k='+str(i)] = zs

pd.plotting.scatter_matrix(csa_pn_df,diagonal='kde',figsize=(8,8))

csa_pn_df.to_csv('csa_tsfam_gi_stat.csv')

msa_pn_df = msa_df_trunc[['GEO','perc_native','census_pop','ts_nh_family','Pop_Density']].copy()
# MSA: k=8
for i in range(1,15):
    GiD,EGi,zs = calc_gi_knn(msa_df_trunc,msa_dists_trunc,i,'ts_nh_family')
    plt.hist(zs)
    plt.title('KNN for K='+str(i))
    plt.show()
    msa_pn_df['ts_nh_family_Zs_k='+str(i)] = zs


pd.plotting.scatter_matrix(msa_pn_df,diagonal='kde',figsize=(8,8))

msa_pn_df.to_csv('msa_tsfam_gi_stat.csv')

#%%


tus = pd.read_csv('MSA_Dataset_Clean.csv', index_col=0)

for i in range(0,130,10):
    tus_filtered = tus[tus['count_TUS'] > i]
    print(i,len(tus_filtered))
    print(stats.pearsonr(tus_filtered['Pop_Density'], tus_filtered['ts_nh_family']))
    
    
#%% Moran's I

def knn_func(dists,tus,k):
    result = {}
    for geo in tus['GEO']:
        neighbors = []
        d = dists[geo].sort_values()
        index = d.index.to_list()
        for ind in index:
            if len(neighbors) < k and tus['GEO'].isin([ind]).any():
                neighbors.append(ind)
        result[geo] = neighbors
    return(result)

def moransi(tus,knn):
    loc_array = tus['GEO'].tolist()
    tus_array = tus['ts_nh_family'].tolist()
    xbar = sum(tus_array)/len(tus_array)
    w = sum([len(v) for k,v in knn.items() if k in loc_array])
    n = len(tus)
    n_w = n/w
    num_sum = 0
    den_sum = 0
    
    for geo_index in range(n):
        xvar = tus_array[geo_index] - xbar
        den_sum += (xvar)**2
        num_sum += sum([xvar*(tus_array[loc_array.index(i)]-xbar) \
                        for i in knn[loc_array[geo_index]]])
    morans_i = n_w*(num_sum/den_sum)
    
    e_i = -1/(n-1)
    
    s1 = sum([len(v)*2**2 for k,v in knn.items() if k in loc_array])*0.5
    # equivalently:
    # s1 = 4*k*len(tus)/2
    s2 = sum([(len(v)+len(v))**2 for k,v in knn.items() if k in loc_array])
    # if that s2 is wrong, s2 = s1*2 may be a simple solution
    s3 = (n**-1 * sum([(i-xbar)**4 for i in tus_array])) / \
         (n**-1 * sum([(i-xbar)**2 for i in tus_array]))**2
    s4 = (n**2-3*n+3)*s1-n*s2+3*w**2    
    s5 = (n**2-n)*s1-2*n*s2+6*w**2
    var_i = (n*s4-s3*s5) / ((n-1)*(n-2)*(n-3)*w**2)
    z = (morans_i - e_i) / var_i**0.5
    #print(morans_i, e_i, var_i, z)
    #print(s1,s2,s3,s4,s5,var_i) 
    return(morans_i, e_i, z)


z_csa,z_msa = [],[]
e_csa,e_msa = [],[]
i_csa,i_msa = [],[]

for i in range(1,31):
    csa_knn = knn_func(csa_dists,csa_tus,i)
    m_i, e_i,z = moransi(csa_tus,csa_knn)
    z_csa.append(z)
    e_csa.append(e_i)
    i_csa.append(m_i)
    #print(i, m_i, e_i, z)
#print()

for i in range(1,31):
    msa_knn = knn_func(msa_dists,msa_tus,i)
    m_i, e_i,z = moransi(msa_tus,msa_knn)
    z_msa.append(z)
    e_msa.append(e_i)
    i_msa.append(m_i)
    #print(i, m_i, e_i, z)
    
#%% Plot Moran's

fig, ax1 = plt.subplots(figsize=(6,4))

ax1.plot(z_csa,label='CSA Z',c='tab:blue')
ax1.plot(z_msa,label='MSA Z',c='tab:orange')
ax1.legend()
ax1.set_ylim((-0.2,3.3))

ax2 = ax1.twinx()
ax2.set_ylim((-0.02,0.15))

#ax2.plot(e_csa,label='CSA E(I)',c='tab:red')
#ax2.plot(e_msa,label='MSA E(I)',c='tab:green')
ax2.plot(i_csa,label='CSA I',c='purple')
ax2.plot(i_msa,c='blue',label='MSA I')
ax1.set_xlabel('K neighbors')
ax1.set_ylabel("Moran's I Z-Scores")
#ax2.set_ylabel("Expected Value and Moran's I Scores")
ax2.set_ylabel("Moran's I Scores")
ax2.legend()
plt.show()

#%% Anselin Moran

def moranslocali(tus,knn):
    loc_array = tus['GEO'].tolist()
    tus_array = tus['ts_nh_family'].tolist()
    tus_mean = sum(tus_array)/len(tus_array)
    tus_std = (sum([((i-tus_mean)**2)/(len(tus_array)-1)]))**0.5
    z_array = [(i-tus_mean)/tus_std for i in tus_array]
    n = len(tus)
    m2 = sum([i**2/n for i in z_array])
    m4 = sum([i**4/n for i in z_array])
    b2 = m4/m2**2
    
    # If this is adapted to use weights != 1, these statements needs to be updated
    w_i2 = [len(knn[i]) for i in loc_array]
    n_nc2 = len(knn[loc_array[0]])
    nc2 = (n_nc2*(n_nc2-1))/2
    w2khi = nc2
    e_i = -n_nc2 / (n-1)
    
    loc_i = []
    z_i = []
    for geo_index in range(n):
        w = 1
        z = z_array[geo_index]
        neighbors = knn[loc_array[geo_index]]
        neighbor_locs = [loc_array.index(i) for i in neighbors]
        neighbor_zs = [z_array[i]*w for i in neighbor_locs]
        
        i_i = (z/m2)*sum(neighbor_zs)
        var_i = (w_i2[geo_index]*(n-b2))/(n-1) + w2khi * (2*b2-n)/((n-1)*(n-2))-w_i2[geo_index] / (n-1)**2
        
        
        
        loc_i.append(i_i)
        z_i.append((i_i-e_i)/var_i**0.5)
        #print(i_i,e_i,var_i**0.5)
        
    return(loc_i,z_i)


csa_pn_df = csa_tus[['GEO','perc_native','census_pop','ts_nh_family','Pop_Density']].copy()

csa_knn = knn_func(csa_dists,csa_pn_df,8)
loc_i,z_i = moranslocali(csa_tus,csa_knn)

#%%
for i in range(1,15):
    csa_knn = knn_func(csa_dists,csa_pn_df,i)
    loc_i,z_i = moranslocali(csa_tus,csa_knn)
    csa_pn_df['ts_nh_family_Loc_I_k='+str(i)] = z_i
    
csa_pn_df.to_csv('csa_tsfam_local_i.csv')

msa_pn_df = msa_tus[['GEO','perc_native','census_pop','ts_nh_family','Pop_Density']].copy()
for i in range(1,15):
    msa_knn = knn_func(msa_dists,msa_pn_df,i)
    loc_i,z_i = moranslocali(msa_tus,msa_knn)
    msa_pn_df['ts_nh_family_Loc_I_k='+str(i)] = z_i

msa_pn_df.to_csv('msa_tsfam_local_i.csv')