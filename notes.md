
Notes on distances to family members

Relocation study
CH Mulder, 2009
https://link.springer.com/article/10.1007/s10901-008-9130-0
 - Netherlands Kinship Panel Study
 - the greater the distance to family members, the less frequent is the contact, particularly at short and medium distances (Lee et al. 1990; Bengtson and Roberts 1991; Grundy and Shelton 2001; Lawton et al. 1994; Smith 1998; Glaser and Tomassini 2000; Hank 2007).
 - The provision of care and support for family members also takes place less often when the distance between the places of residence is greater (Daatland and Lowenstein 2005; De Jong Gierveld and Fokkema 1998; Knijn and Liefbroer 2006; Joseph and Hallman 1998; Tomassini et al. 2003)
 - Relocation rates of older adults are low. Older adults are usually satisfied with their home and residential situation and have no wish to move away from their familiar surroundings (Rogerson et al. 1997)
 - Family members are very important in the contact and support networks of older adults (Miner and Uhlenberg 1997; De Jong Gierveld and Fokkema 1998)
 - At the same time, older adults are also important providers of support (Morgan et al. 1991; Van Tilburg et al. 1995; Lin and Rogerson 1995; Remery et al. 2000). Particularly around retirement age, when less or no time is spent in paid work but most older adults are still in good health, older adults have good opportunities to provide support to their family members. Those with grandchildren are frequently involved in childcare, and usually desire to spend time with their grandchildren and to participate in their lives.

CH Mulder, 2009
https://onlinelibrary.wiley.com/doi/abs/10.1002/psp.557