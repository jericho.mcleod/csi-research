'''
Relevant Sources:

COHEN, W.M., R.C. LEVIN and D.C. MOWERY (I 987), “Firm size and R&D intensity: A reexamination”,
journal of Industrial Economics, 35, pp. 543-563.

County distances:
    https://www.nber.org/research/data/county-distance-database

Trips by distance:
    https://www.bts.gov/daily-travel
    
Crosswalk:
    https://www.bls.gov/cew/classifications/areas/county-msa-csa-crosswalk.htm


Census Regions
https://www2.census.gov/geo/pdfs/maps-data/maps/reference/us_regdiv.pdf


KNN + Getis Ord G
    https://journals-sagepub-com.mutex.gmu.edu/doi/10.1177/0023830919894720

Spatial autocorrelation book
    https://www.cambridge.org/core/books/abs/spatial-analysis-methods-and-practice/spatial-autocorrelation/F6A01B574C69076F28318445C33397E4

Moran's I:
    https://www-jstor-org.mutex.gmu.edu/stable/2332142?origin=crossref&seq=1#metadata_info_tab_contents

'''

import pandas as pd
import numpy as np
import random


def rand_array(a):
    b = []
    while len(a) > 0:
        b.append(a.pop(random.randint(0,len(a)-1)))
        
    for i in range(len(a)):
        print(a[i],b[i])
    return b


def nr(n,r): #todo verify this
    x = 1
    for i in range(1,r+1):
        x = x*(n-i+1)
    return x


def calc_ms(df,var):
    ms =   {'m1': sum([i for i in df[var].tolist()]),
            'm2': sum([i**2 for i in df[var].tolist()]),
            'm3': sum([i**3 for i in df[var].tolist()]),
            'm4': sum([i**4 for i in df[var].tolist()])}


def getis(geo_tus,geo_dists,ms,target_dist,var,rand=0):
    geo_list = geo_tus['GEO'].tolist()
    x = geo_tus[var].tolist()
    if rand == 1:
        x = rand_array(x)
    elif rand == 2:
        x = [random.random() for i in x]
    geo_ref = geo_dists.columns.tolist()
    weighted_sum = 0
    unweighted_sum = 0
    weights = 0
    weights_array = []
    for i in range(len(geo_list)):
        temp_weights = 0
        for j in range(len(geo_list)):
            if i!=j:
                unweighted_sum += x[i]*x[j]
                if geo_dists.iloc[geo_ref.index(geo_list[i])][geo_list[j]] <= target_dist:
                    weighted_sum += x[i]*x[j]
                    weights += 1
                    temp_weights += 1
        weights_array.append(temp_weights)
    Gd = weighted_sum/unweighted_sum
    EGd = weights / (len(x)*(len(x)-1))
    S1 = weights *2
    S2 = sum([(i*2)**2 for i in weights_array])
    n = len(x)
    n2 = nr(n,2)
    n4 = nr(n,4)
    B0 = (n2 - 3*n + 3) * S1 - n*S2 + 3*weights**2
    B1 = -((n2-n)*S1-2*n*S2+3*weights**2)
    B2 = -(2*n*S1 - (n+3)*S2 + 6*weights**2)
    B3 = 4*(n-1)*S1-2*(n+1)*S2+8*weights**2
    B4 = S1-S2+weights**2

    EG2 = (1/((ms['m1']**2 - ms['m2'])**2 * n4)) *(B0*ms['m2']**2 +\
                                                 B1*ms['m4']+\
                                                 B2*ms['m1']**2*ms['m2']+\
                                                 B3*ms['m1']*ms['m3']+\
                                                 B4*ms['m1']**4)

    var_g = EG2 - (weights / n2)**2
    z = (Gd-EGd) / var_g**0.5
    return(z)


def calc_gi(data,dists,target_dist,param):
    import numpy as np
    GiD = []
    EGi = []
    geo_list = data['GEO'].tolist()
    x = data[param].tolist()
    geo_ref = dists.columns.tolist()
    var = []
    zs = []
    for i in range(len(geo_list)):
        w_sum = 0
        uw_sum = 0
        w_count = 0
        xj = 0
        xjsq = 0
        for j in range(len(geo_list)):
            uw_sum += x[j]#x[i]*x[j]
            xj += x[j]
            xjsq += x[j]**2
            if dists.iloc[geo_ref.index(geo_list[i])][geo_list[j]] <= target_dist:
                w_sum += x[j]# x[i]*x[j]
                w_count += 1
        GiD.append(w_sum/uw_sum)
        EGi.append(w_count/(len(geo_list)-1))
        y1 = xj / (len(geo_list)-1)
        y2 = xjsq / (len(geo_list)-1) - y1**2
        var = w_count * (len(geo_list) - 1 - w_count) / ((len(geo_list)-1)**2 * (len(geo_list)-2)) * (y2/y1**2)
        try:
            z = (GiD[-1]-EGi[-1])/var**0.5
        except:
            z = np.nan
        zs.append(z)
    return(GiD,EGi,zs)


def calc_rand_gi(data,dists,target_dist,param):
    import numpy as np
    GiD = []
    EGi = []
    geo_list = data['GEO'].tolist()
    x = data[param].tolist()
    geo_ref = dists.columns.tolist()
    var = []
    zs = []
    x = rand_array(x)
    for i in range(len(geo_list)):
        w_sum = 0
        uw_sum = 0
        w_count = 0
        xj = 0
        xjsq = 0
        for j in range(len(geo_list)):
            uw_sum += x[j]#x[i]*x[j]
            xj += x[j]
            xjsq += x[j]**2
            if dists.iloc[geo_ref.index(geo_list[i])][geo_list[j]] <= target_dist:
                w_sum += x[j]# x[i]*x[j]
                w_count += 1
        GiD.append(w_sum/uw_sum)
        EGi.append(w_count/(len(geo_list)-1))
        y1 = xj / (len(geo_list)-1)
        y2 = xjsq / (len(geo_list)-1) - y1**2
        var = w_count * (len(geo_list) - 1 - w_count) / ((len(geo_list)-1)**2 * (len(geo_list)-2)) * (y2/y1**2)
        try:
            z = (GiD[-1]-EGi[-1])/var**0.5
        except:
            z = np.nan
        zs.append(z)
    return(GiD,EGi,zs)


def above_below(a,b):
    a_greater = []
    for i in range(len(a)):
        if a[i] > b[i]:
            a_greater.append(1)
        else:
            a_greater.append(0)
    return(a_greater)


def calc_gi_kth(data,dists,param):
    import numpy as np
    GiD = []
    EGi = []
    geo_list = data['GEO'].tolist()
    dist_list = data['K_dist'].tolist()
    x = data[param].tolist()
    geo_ref = dists.columns.tolist()
    var = []
    zs = []
    for i in range(len(geo_list)):
        w_sum = 0
        uw_sum = 0
        w_count = 0
        xj = 0
        xjsq = 0
        target_dist = dist_list[i]
        for j in range(len(geo_list)):
            uw_sum += x[j]#x[i]*x[j]
            xj += x[j]
            xjsq += x[j]**2
            if dists.iloc[geo_ref.index(geo_list[i])][geo_list[j]] <= target_dist:
                w_sum += x[j]# x[i]*x[j]
                w_count += 1
        GiD.append(w_sum/uw_sum)
        EGi.append(w_count/(len(geo_list)-1))
        y1 = xj / (len(geo_list)-1)
        y2 = xjsq / (len(geo_list)-1) - y1**2
        var = w_count * (len(geo_list) - 1 - w_count) / ((len(geo_list)-1)**2 * (len(geo_list)-2)) * (y2/y1**2)
        try:
            z = (GiD[-1]-EGi[-1])/var**0.5
        except:
            z = np.nan
        zs.append(z)
    return(GiD,EGi,zs)


def calc_rand_gi_kth(data,dists,param):
    import numpy as np
    GiD = []
    EGi = []
    geo_list = data['GEO'].tolist()
    dist_list = data['K_dist'].tolist()
    x = data[param].tolist()
    geo_ref = dists.columns.tolist()
    var = []
    zs = []
    x = rand_array(x)
    for i in range(len(geo_list)):
        w_sum = 0
        uw_sum = 0
        w_count = 0
        xj = 0
        xjsq = 0
        target_dist = dist_list[i]
        for j in range(len(geo_list)):
            uw_sum += x[j]#x[i]*x[j]
            xj += x[j]
            xjsq += x[j]**2
            if dists.iloc[geo_ref.index(geo_list[i])][geo_list[j]] <= target_dist:
                w_sum += x[j]# x[i]*x[j]
                w_count += 1
        GiD.append(w_sum/uw_sum)
        EGi.append(w_count/(len(geo_list)-1))
        y1 = xj / (len(geo_list)-1)
        y2 = xjsq / (len(geo_list)-1) - y1**2
        var = w_count * (len(geo_list) - 1 - w_count) / ((len(geo_list)-1)**2 * (len(geo_list)-2)) * (y2/y1**2)
        try:
            z = (GiD[-1]-EGi[-1])/var**0.5
        except:
            z = np.nan
        zs.append(z)
    return(GiD,EGi,zs)


def kth_n_dist(geo_df,dist_df,k):
    import numpy as np
    geo_array = geo_df['GEO'].tolist()
    geo_index = dist_df.index.tolist()
    dist = []
    for g in geo_array:
        if g in geo_index:
            row = sorted(dist_df.iloc[geo_index.index(g)].replace(np.nan,1000000).tolist())
            d = row[k-1]
            dist.append(d)
        else:
            dist.append(0)
    out_df = copy.deepcopy(geo_df)
    out_df['K_dist'] = dist
    return(out_df)
